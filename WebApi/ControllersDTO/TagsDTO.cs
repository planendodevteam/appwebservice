﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.ControllersDTO
{
    public class TagsDTO
    {
        public int Id_Tag { get; set; }
        public long? Id_Plan { get; set; }
        public String Tag_Name { get; set; }
        public long? Created_By { get; set; }
        public bool? Active { get; set; }

        public TagsDTO() { }

        public TagsDTO(Tags_x_Plan tg)
        {
            this.Id_Tag = tg.Id_Tag;
            this.Id_Plan = tg.Id_Plan;
            this.Tag_Name = tg.Tag_Name;
            this.Created_By = tg.Created_By;
            this.Active = tg.Active;
        }
    }
}