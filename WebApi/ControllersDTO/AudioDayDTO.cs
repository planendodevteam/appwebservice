﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.ControllersDTO
{
    public class AudioDayDTO
    {
        public long idPlan { get; set; }
        public int idNumDay { get; set; }
        public string audioName { get; set; }
        public string audioPath { get; set; }
        public string audioFile { get; set; }

        public AudioDayDTO() { }

        public AudioDayDTO(Stored_Recording sr) {
            this.idNumDay = sr.Id_Day_Plan_Num;
            this.idPlan = sr.Id_Plan;
            this.audioName = sr.AudioName;
            this.audioFile = sr.AudioFile;
            this.audioPath = sr.AudioPath;

            if (this.audioName != null) { this.audioName = this.audioName.Trim(); }
            if (this.audioFile != null) { this.audioFile = this.audioFile.Trim(); }
            if (this.audioPath != null) { this.audioPath = this.audioPath.Trim(); }

        }
    }
}