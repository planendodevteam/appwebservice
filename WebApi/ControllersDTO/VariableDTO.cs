﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.ControllersDTO
{
    public class VariableDTO
    {
        public String Id_Var { get; set; }
        public String Name { get; set; }
        public String Value { get; set; }

        public VariableDTO() { }

        public VariableDTO(Variables var)
        {
            this.Id_Var = var.Id_Var;
            this.Name = var.Name;
            this.Value = var.Value;
        }
    }
}