﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.ControllersDTO
{
    public class PlanDTO
    {
        public long IdPlan { get; set; }
        public long IdSpecialist { get; set; }
        public long IdUser { get; set; }
        public long? idPlanMaster { get; set; }
        public long IdCompany { get; set; }
        public int IdProgression { get; set; }
        public int IdLevel { get; set; }
        public int IdProgramType { get; set; }
        public int IdPlanAcces { get; set; }
        public int IdPlanStatus { get; set; }
        public bool? infoSpecialist { get; set; }
        public bool Active { get; set; }
        public string PlanName { get; set; }
        public string picture { get; set; }
        public DateTime Created { get; set; }
        public DateTime StartForecast { get; set; }
        public DateTime EndForecast { get; set; }
        public DateTime ActualStart { get; set; }
        public DateTime ActualEnd { get; set; }
        public DateTime? LastDay { get; set; }
        public int DaysDuration { get; set; }
        public long LastExercise { get; set; }
        public string IdLang { get; set; }
        public string SpecialistName { get; set; }
        public bool? SpecialistVerificateUniv { get; set; }
        public string CompanyName { get; set; }
        public string UserName { get; set; }
        public string PlanStatus { get; set; }
        public double Progression { get; set; }
        public string ProgressionText { get; set; }
        public string imgProgramType { get; set; }
        public string imgStatus { get; set; }
        public string imgSpecialist { get; set; }
        public string imgCompany { get; set; }
        public string TypeProgramName { get; set; }
        public string ProgressionImg { get; set; }
        public string TimeSpent { get; set; }
        public string Total_Time_Spent { get; set; }
        public int progressStatus { get; set; }
        public string progressStatusText { get; set; }
        public string Message { get; set; }
        public double? price { get; set; }
        public bool? free { get; set; }
        public bool? like { get; set; }
        public bool? marketplaceTop { get; set; }
        public string linkSpecialist { get; set; }
        public List<DayDTO> ldays { get; set; }
        public List<Stored_Ex_Plan_DTO> lex { get; set; }
        public List<TagsDTO> tagList { get; set; }

        public PlanDTO() { }

        public PlanDTO(Plans pl)
        {
            this.PlanName = pl.Plan_Name_Stored;
            this.Active = pl.Active.GetValueOrDefault();
            this.ActualEnd = pl.Actual_End_Datetime.GetValueOrDefault();
            this.ActualStart = pl.Actual_Start_Datetime.GetValueOrDefault();
            this.CompanyName = pl.Company_TradeName_Stored;
            this.Created = pl.Created_Datetime.GetValueOrDefault();
            this.DaysDuration = pl.NumDays_Duration.GetValueOrDefault();
            this.EndForecast = pl.Forecast_End_Date.GetValueOrDefault();
            this.IdCompany = pl.Id_Company.GetValueOrDefault();
            this.IdLevel = pl.Id_Level.GetValueOrDefault();
            this.IdPlan = pl.Id_Plan;
            this.IdPlanStatus = pl.Id_PlanStatus.GetValueOrDefault();
            this.IdProgression = pl.Id_Progression.GetValueOrDefault();
            this.IdSpecialist = pl.Id_Specialist.GetValueOrDefault();
            this.IdUser = pl.Id_User.GetValueOrDefault();
            this.LastDay = pl.Last_Day.GetValueOrDefault();
            this.LastExercise = pl.Last_Exercise_Id.GetValueOrDefault();
            this.SpecialistName = pl.Specialist_Name_Stored;
            this.StartForecast = pl.Forecast_Start_Date.GetValueOrDefault();
            this.UserName = pl.User_Name_Stored;
            this.IdPlanAcces = pl.Id_PlanAccess.GetValueOrDefault();
            this.IdProgramType = pl.Id_ProgramType.GetValueOrDefault();
            this.Message = pl.Message;
            this.price = pl.Price;
            this.free = pl.free;
            this.idPlanMaster = pl.Id_Plan_Master;
            this.picture = pl.Picture;
            this.marketplaceTop = pl.Marketplace_Top;
            this.infoSpecialist = pl.InfoESpecialist;

            if (this.PlanName != null) { this.PlanName = this.PlanName.Trim(); }
            if (pl.Id_Lang != null) { this.IdLang = pl.Id_Lang.Trim(); }
            if (this.UserName != null) { this.UserName = this.UserName.Trim(); }
            if (this.SpecialistName != null) { this.SpecialistName = this.SpecialistName.Trim(); }
            if (this.CompanyName != null) { this.CompanyName = this.CompanyName.Trim(); }
        }
    }
}