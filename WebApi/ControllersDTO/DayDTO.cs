﻿using RCPT.AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.ControllersDTO
{
    public class DayDTO
    {
        public long idPlan { get; set; }
        //public long? idPlanMaster { get; set; }
        public int idNumDay { get; set; }
        public string Status { get; set; }
        public int? status_id { get; set; }
        public string nameDay { get; set; }
        public string comment { get; set; }
        public string audioName { get; set; }
        public string audioPath { get; set; }
        public string audioFile { get; set; }
        public int? Feedback_user_rating { get; set; }
        public int? Feedback_user_dificulty { get; set; }
        public DateTime? Daycompleted { get; set; }
        public TimeSpan? Total_Duration { get; set; }
        public string Total_Time_Spent { get; set; }
        public string DayCompletedText { get; set; }
        public double? Duration { get; set; }
        public List<ElementsCS> elements { get; set; }
        public List<Stored_Ex_Plan_DTO> lex { get; set; }

        public DayDTO() { }

        public DayDTO(Stored_Plans_x_Day Stored_day)
        {
            this.idPlan = Stored_day.Id_Plan;
            this.idNumDay = Stored_day.Id_Day_Plan_Num;
            this.status_id = Stored_day.Status_Id;
            this.Feedback_user_dificulty = Stored_day.Feedback_User_Dificulty_Id;
            this.Feedback_user_rating = Stored_day.Feedback_User_Rating_Id;
            this.Total_Duration = Stored_day.Total_Duration;
            this.Daycompleted = Stored_day.Date_Completed;
            this.comment = Stored_day.Comment;
            //this.DayCompletedText = DiesRepository.GetTextDayCompleted(this, idLang);
            if (this.Total_Duration != null)
            {
                TimeSpan ts = (TimeSpan)this.Total_Duration;
                this.Duration = ts.TotalMinutes;
                this.Duration = Math.Round((double)this.Duration, 2);
            }
        }

        public DayDTO(long idPlan, int numDay)
        {
            this.idPlan = idPlan;
            this.idNumDay = numDay;
        }
    }
}