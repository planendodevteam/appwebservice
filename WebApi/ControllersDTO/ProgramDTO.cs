﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.ControllersDTO
{
    public class ProgramDTO
    {
        public long idProgram { get; set; }
        public long idSpecialist { get; set; }
        public long idCompany { get; set; }
        public int? idAccesLevel { get; set; }
        public int? idProgramType { get; set; }
        public bool? isMaster { get; set; }
        public string programName { get; set; }
        public string specialistName { get; set; }
        public string specialistStatus { get; set; }
        public string companyName { get; set; }
        public string logoCompany { get; set; }
        public string imgProgramType { get; set; }
        public string imgSpecialist { get; set; }
        public string programTypeName { get; set; }
        public double? price { get; set; }
        public bool? free { get; set; }
        public string currency { get; set; }
        public string programDefinition { get; set; }
        public string specialistPresentation { get; set; }
        public int? rating { get; set; }
        public int views { get; set; }
        public bool? isPremium { get; set; }
        public bool? membership { get; set; }
        public List<Stored_Ex_Plan_DTO> lex { get; set; }


        public ProgramDTO(Programs p)
        {
            this.idProgram = p.Id_Program;
            this.idAccesLevel = p.Id_AccesLevel;
            this.idProgramType = p.Id_ProgramType;
            this.idCompany = p.Id_Company;
            this.isMaster = p.IsMaster;
            this.idSpecialist = p.Created_by_Specialist;
            this.programName = p.Programs_TXT.Select(x => x.Name).FirstOrDefault();
            this.logoCompany = p.Companies.DesignTemplates.Select(x => x.Logo_B64).FirstOrDefault();
            this.companyName = p.Companies.Company_Tradename;
            this.currency = "EUR";
            this.programDefinition = "Plan de planendo gratuïto";
            this.specialistPresentation = "Especialista colegiado y graduado.";
            this.views = 3540;
            this.isPremium = p.Premium;

            if (p.Programs_Pricing != null) { this.free = p.Programs_Pricing.Free; this.price = p.Programs_Pricing.Price; }
            if (p.Programs_Rating != null) { this.rating = p.Programs_Rating.Rating; }
            
            if (this.companyName != null)
            {
                this.companyName = this.companyName.Trim();
            }
            if (this.programName != null)
            {
                this.programName = this.programName.Trim();
            }
            if (this.logoCompany != null)
            {
                this.logoCompany = this.logoCompany.Trim();
            }
        }
    }
}