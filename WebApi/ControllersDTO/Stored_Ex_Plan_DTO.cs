﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.ControllersDTO
{

    public class Stored_Ex_Plan_DTO
    {
        public long idPlan { get; set; }
        public int idWorkPhase { get; set; }
        public int idWork { get; set; }
        public int? idLevel { get; set; }
        public int? idLevel_series { get; set; }
        public long idEx { get; set; }
        public long idProgram { get; set; }
        public string WorkPhaseName_Stored { get; set; }
        public string WorkPhaseName { get; set; }
        public string ExerciseName { get; set; }
        public int? idUnit { get; set; }
        public int? idProgression { get; set; }
        public int UnitNum { get; set; }
        public int? SeriesNum { get; set; }
        public int? WeightNum { get; set; }
        public string LinkVideoMaleAdult { get; set; }
        public string LinkVideoFemaleAdult { get; set; }
        public int ExNumDayStart { get; set; }
        public int? ExNumDaysDuration { get; set; }
        public string ProgramName { get; set; }
        public string pictureMale { get; set; }
        public string pictureFemale { get; set; }
        public int? orderNum { get; set; }
        public string UnitName { get; set; }
        public string WorkName { get; set; }
        public bool? isRealized { get; set; }
        public DateTime? Start_DateTime { get; set; }

        public Stored_Ex_Plan_DTO() { }

        public Stored_Ex_Plan_DTO(Stored_Exercises_x_Plan ex)
        {
            this.idPlan = ex.Id_Plan;
            this.idWorkPhase = ex.Id_WorkPhase;
            this.idWork = ex.Id_Work;
            this.idEx = ex.Id_Exercise;
            this.idProgram = ex.Id_Program;
            this.idUnit = ex.Id_Unit.GetValueOrDefault();
            this.UnitNum = ex.Unit_Num_Stored.GetValueOrDefault();
            this.SeriesNum = ex.Series_Num_Stored.GetValueOrDefault();
            this.WeightNum = ex.Weight_Num_Stored.GetValueOrDefault();
            if (ex.Exercises != null)
            {
                this.LinkVideoMaleAdult = ex.Exercises.Link_Video_MaleAdult.Trim();
                this.LinkVideoFemaleAdult = ex.Exercises.Link_Video_FemaleAdult.Trim();
                this.pictureFemale = ex.Exercises.Link_Picture_FemaleAdult.Trim();
                this.pictureMale = ex.Exercises.Link_Picture_MaleAdult.Trim();
            }
            if (ex.WorkPhase_Name_Stored != null) { this.WorkPhaseName_Stored = ex.WorkPhase_Name_Stored.Trim(); }
            if (ex.Exercise_Name_Stored != null) { this.ExerciseName = ex.Exercise_Name_Stored.Trim(); }
            if (ex.Program_Name_Stored != null) { this.ProgramName = ex.Program_Name_Stored.Trim(); }

            this.ExNumDaysDuration = ex.Exercise_NumDays_Duration.GetValueOrDefault();
            this.ExNumDayStart = ex.Exercise_NumDay_Start.GetValueOrDefault();

            //this.orderNum = ex.WorkPhases.OrderNum.GetValueOrDefault();
        }

        public Stored_Ex_Plan_DTO(Stored_Exercises_x_Plan ex, int numDay, string idLang)
        {
            this.idPlan = ex.Id_Plan;
            this.idWorkPhase = ex.Id_WorkPhase;
            this.idWork = ex.Id_Work;
            this.idEx = ex.Id_Exercise;
            this.idProgram = ex.Id_Program;
            this.idUnit = ex.Id_Unit.GetValueOrDefault();
            if (ex.WorkPhase_Name_Stored != null) { this.WorkPhaseName_Stored = ex.WorkPhase_Name_Stored.Trim(); }
            if (ex.Exercise_Name_Stored != null) { this.ExerciseName = ex.Exercise_Name_Stored.Trim(); }
            if (ex.Program_Name_Stored != null) { this.ProgramName = ex.Program_Name_Stored.Trim(); }
            this.UnitNum = ex.Unit_Num_Stored.GetValueOrDefault();
            this.SeriesNum = ex.Series_Num_Stored.GetValueOrDefault();
            this.WeightNum = ex.Weight_Num_Stored.GetValueOrDefault();
            if (ex.Exercises != null)
            {
                this.LinkVideoMaleAdult = ex.Exercises.Link_Video_MaleAdult.Trim();
                this.LinkVideoFemaleAdult = ex.Exercises.Link_Video_FemaleAdult.Trim();
                this.pictureFemale = ex.Exercises.Link_Picture_FemaleAdult.Trim();
                this.pictureMale = ex.Exercises.Link_Picture_MaleAdult.Trim();
            }
            this.ExNumDaysDuration = ex.Exercise_NumDays_Duration.GetValueOrDefault();
            this.ExNumDayStart = ex.Exercise_NumDay_Start.GetValueOrDefault();
            this.orderNum = ex.WorkPhases.OrderNum.GetValueOrDefault();
            this.WorkPhaseName = ex.WorkPhases.WorkPhases_TXT.Where(x => x.Id_WorkPhase == this.idWorkPhase).Where(x => x.Id_Lang == idLang).Select(z => z.Name).FirstOrDefault().Trim();
            this.UnitName = ex.Units.Units_TXT.Where(x => x.Id_Lang == idLang).Select(z => z.Name).FirstOrDefault().Trim();

            this.Start_DateTime = ex.Plans.Stored_Plans_Exercises_x_Day.Where(x => x.Id_Day_Plan_Num == numDay && x.Id_Plan == ex.Id_Plan &&
            x.Id_Exercise == ex.Id_Exercise && x.Id_Program == ex.Id_Program & x.Id_Workphase == ex.Id_WorkPhase && x.Id_Work == ex.Id_Work)
                .Select(z => z.Datetime_watched).FirstOrDefault();

            if (ex.Plans.Stored_Plans_Exercises_x_Day.Where(x => x.Id_Day_Plan_Num == numDay && x.Id_Plan == ex.Id_Plan
               && x.Id_Program == ex.Id_Program & x.Id_Workphase == ex.Id_WorkPhase && x.Id_Work == ex.Id_Work)
                .Select(z => z.Datetime_watched).FirstOrDefault() != null)
            { this.isRealized = true; }

            else if (ex.Plans.Stored_Plans_Exercises_x_Day.Where(x => x.Id_Day_Plan_Num == numDay && x.Id_Plan == ex.Id_Plan
               && x.Id_Program == ex.Id_Program & x.Id_Workphase == ex.Id_WorkPhase && x.Id_Work == ex.Id_Work)
                .Select(z => z.Datetime_watched).FirstOrDefault() == null
                && ex.Plans.Stored_Plans_Exercises_x_Day.Where(x => x.Id_Day_Plan_Num == numDay && x.Id_Plan == ex.Id_Plan
               && x.Id_Program == ex.Id_Program & x.Id_Workphase == ex.Id_WorkPhase && x.Id_Work == ex.Id_Work)
                .Select(z => z.Datetime_watched).FirstOrDefault() != null)
            { this.isRealized = false; }

            else { this.isRealized = null; }

        }

        public Stored_Ex_Plan_DTO(Stored_Exercises_x_Plan ex, string idLang)
        {
            this.idPlan = ex.Id_Plan;
            this.idWorkPhase = ex.Id_WorkPhase;
            this.idWork = ex.Id_Work;
            this.idEx = ex.Id_Exercise;
            this.idProgram = ex.Id_Program;
            this.idUnit = ex.Id_Unit.GetValueOrDefault();
            if (ex.WorkPhase_Name_Stored != null) { this.WorkPhaseName_Stored = ex.WorkPhase_Name_Stored.Trim(); }
            if (ex.Exercise_Name_Stored != null) { this.ExerciseName = ex.Exercise_Name_Stored.Trim(); }
            if (ex.Program_Name_Stored != null) { this.ProgramName = ex.Program_Name_Stored.Trim(); }
            this.UnitNum = ex.Unit_Num_Stored.GetValueOrDefault();
            this.SeriesNum = ex.Series_Num_Stored.GetValueOrDefault();
            this.WeightNum = ex.Weight_Num_Stored.GetValueOrDefault();
            if (ex.Exercises != null)
            {
                this.LinkVideoMaleAdult = ex.Exercises.Link_Video_MaleAdult.Trim();
                this.LinkVideoFemaleAdult = ex.Exercises.Link_Video_FemaleAdult.Trim();
                this.pictureFemale = ex.Exercises.Link_Picture_FemaleAdult.Trim();
                this.pictureMale = ex.Exercises.Link_Picture_MaleAdult.Trim();
            }
            this.ExNumDaysDuration = ex.Exercise_NumDays_Duration.GetValueOrDefault();
            this.ExNumDayStart = ex.Exercise_NumDay_Start.GetValueOrDefault();
            this.orderNum = ex.WorkPhases.OrderNum.GetValueOrDefault();
            this.WorkPhaseName = ex.WorkPhases.WorkPhases_TXT.Where(x => x.Id_WorkPhase == this.idWorkPhase && x.Id_Lang == idLang).Select(z => z.Name).FirstOrDefault().Trim();
            this.UnitName = ex.Units.Units_TXT.Where(x => x.Id_Lang == idLang).Select(z => z.Name).FirstOrDefault().Trim();
        }
    }
}