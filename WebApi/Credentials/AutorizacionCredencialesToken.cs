﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;
using System.Security.Claims;
using WebApi.Models;

namespace WebApi.Credentials
{
    public class AutorizacionCredencialesToken : OAuthAuthorizationServerProvider
    {
        private static Planendo_Test_Entities db = Controlador.db;

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            try
            {
                bool validate = new bool();
                validate = (bool)db.PwdValidateEmail(context.UserName, context.Password).FirstOrDefault();

                if (validate)
                {
                    User newUser = db.User.Where(x => x.Email.Trim().Equals(context.UserName)).FirstOrDefault();

                    if (newUser != null && newUser.Active == true && newUser.Full_Registered_Date != null)
                    {
                        ClaimsIdentity identidad = new ClaimsIdentity(context.Options.AuthenticationType);
                        //identidad.AddClaim(new Claim(ClaimTypes.Name, context.Password));
                        identidad.AddClaim(new Claim(ClaimTypes.Role, "USER"));
                        identidad.AddClaim(new Claim(ClaimTypes.Email, context.UserName));
                        //identidad.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                        context.Validated(identidad);

                    }
                    //Not found
                    else if (newUser == null)
                    {
                        context.SetError("NOTFOUND", "User not found");
                    }
                    //End register
                    else if (newUser.Active == false || newUser.Full_Registered_Date == null)
                    {
                        context.SetError("ENDREGISTER", "User not active");
                    }
                    else
                    {
                        context.SetError("OTHERERROR", "Unspecific error");
                    }
                }
                else if (!validate)
                {
                    bool isEmailExisting = db.User.Where(x => x.Email.Trim().Equals(context.UserName)).FirstOrDefault() != null;
                    
                    if (isEmailExisting)
                    {
                        context.SetError("WRONG_CREDENTIALS","User email found, but wrong password");
                    } else
                    {
                        context.SetError("NOTFOUND", "User not found");
                    }                    
                }
                else
                {
                    context.SetError("OTHERERROR", "Unspecific error");
                }
            }
            catch (Exception e)
            {
                string er = e.Message.ToString();
                context.SetError("OTHERERROR", "Unspecific error");
            }
        }
    }
}