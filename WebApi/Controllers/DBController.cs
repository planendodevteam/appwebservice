﻿using RCPT.AccesoDatos;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;
using User = RCPT.AccesoDatos.User;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using System;

namespace WebApi.Controllers
{
    public class DBController : ApiController
    {
        [Route("getAPI")]
        public HttpResponseMessage GetAPI()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "1_48");
            return response;
        }



        [Route("getCatalog")]
        public HttpResponseMessage GetCatalog()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Planendo_Test_Entities"].ConnectionString;

            if (connectionString.ToLower().StartsWith("metadata="))
            {
                System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder efBuilder = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder(connectionString);
                connectionString = efBuilder.ProviderConnectionString;
            }
            DbConnection connection = new SqlConnection(connectionString);
            var database = connection.DataSource;

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, database);
            return response;
        }


        [Route("getLang")]
        public HttpResponseMessage GetLang()
        {
            var Idioma = LanguagesRepository.GetAll();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Idioma);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("getMenu/{idL}")]
        [HttpGet]
        public HttpResponseMessage getMenu(string idL)
        {
            var menu = LanguagesRepository.GetMenu(idL);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, menu);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("getTypePlans/{idL}")]
        [HttpGet]
        public HttpResponseMessage getTypePlans(string idL)
        {
            var plans = Plan_Repository.GetTypeProgram(idL);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, plans);

            return response;
        }

        [Authorize(Roles = "USER")]
        //Labels, idLanguage
        [Route("getMenu/{name}/{name2}")]
        [HttpGet]
        public HttpResponseMessage getMenu(string name, string name2)
        {
            var menu = LanguagesRepository.MenuByidL(name, name2);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, menu);

            return response;
        }

        //Labels, idLanguage
        [Route("getLabels/{paramOne}/{paramTwo}")]
        public HttpResponseMessage getLabels(string paramOne, string paramTwo)
        {
            var labels = LanguagesRepository.GetLabels(paramOne, paramTwo);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, labels);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("AuthUserToken")]
        [HttpPost]
        public HttpResponseMessage UserToken([FromBody]User user)
        {
            var usuari = UserRepository.AuthUserToken(user.nom);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, usuari);

            return response;
        }


        // idUser , the new language
        [Route("changeUserLang")]
        [HttpPut]
        public HttpResponseMessage ChangeLangPost([FromBody]changeLang c)
        {
            var changelang = UserRepository.changeLang(c.idUser, c.lang);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, changelang);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("changeUserPicture")]
        
        public HttpResponseMessage ChangeProfilePicPost([FromBody]imgCS img)
        {
            var changepic = UserRepository.changePhoto(img.id_user, img.img);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, changepic);

            return response;
        }

        //Change the user password
        [Route("changePassword")]
        [HttpPut]
        public HttpResponseMessage ChangePassword([FromBody]changePasswordCS changePass)
        {
            var control = UserRepository.changePassword(changePass.email, changePass.code, changePass.password);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, control);

            return response;
        }

        //code, email
        [Route("checkVerificationCode")]
        [HttpPost]
        public HttpResponseMessage CheckCode([FromBody]VerificationCode verif)
        {
            var verification = UserRepository.checkVerificationCode(verif.code, verif.email);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, verification);

            return response;
        }

        //Email , idLang
        [Route("CheckEmailRegister/{email}/{idLang}")]
        [HttpGet]
        public HttpResponseMessage CheckEmailRegister(string email, string idLang)
        {
            var Acces = UserRepository.checkEmailRegister(email, idLang);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Acces);

            return response;
        }

        //Email , idLang
        [Route("CheckEmailResetPass/{email}/{idLang}")]
        [HttpGet]
        public HttpResponseMessage CheckEmailResetPass(string email, string idLang)
        {
            var Acces = UserRepository.CheckEmailResetPass(email, idLang);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Acces);

            return response;
        }

        //Edit user info
        [Authorize(Roles = "USER")]
        [Route("editUser")]
        [HttpPut]
        public HttpResponseMessage editUser([FromBody]UserCS user)
        {
            var date = Convert.ToDateTime(user.birthdate);
            var edit = DBChanges.editUser(user.idUser, user.name, user.surname, date, user.lang, user.gender);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, edit);

            return response;
        }

        [Route("UserRegister")]
        [HttpPut]
        public HttpResponseMessage register([FromBody]RegisterCS register)
        {
            var done = UserRepository.UserRegister(register.lang, register.name, register.surname, register.email, register.genre, register.birthdate, register.password);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, done);

            return response;
        }

        //All Exercises of the idPlan and Language
        [Route("ExsOfPlan/{idPlan}/{idLang}")]
        [HttpGet]
        public HttpResponseMessage ExsOfPlan(int idPlan, string idLang)
        {
            var exs = Repository.AllExofPlan(idPlan, idLang);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, exs);

            return response;
        }

        //Get One Plan Specific (bools control if you wanna get the info or not)
        [Authorize(Roles = "USER")]
        [Route("GetPlan/{idUser}/{idPlan}/{idLang}/{Gender}/{DaysContr}/{ExsContr}")]
        [HttpGet]
        public HttpResponseMessage GetPlan(long idUser, long idPlan, string idLang, string Gender, bool DaysContr, bool ExsContr)
        {
            var OnePlan = Plan_Repository.GetPlan(idUser, idPlan, idLang, Gender, DaysContr, ExsContr);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, OnePlan);

            return response;
        }

        [Authorize(Roles = "USER")]
        //Get Plans (bools control if you wanna get the info or not, the PlansControl is for Plans Active/Inactive)
        [Route("GetPlans/{idUser}/{idLang}/{Gender}/{PlansContr}/{DaysContr}/{ExsContr}")]
        [HttpGet]
        public HttpResponseMessage GetPlans(long idUser, string idLang, string Gender, bool PlansContr, bool DaysContr, bool ExsContr)
        {
            var AllPlans = Plan_Repository.GetPlans(idUser, idLang, Gender, PlansContr, DaysContr, ExsContr);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AllPlans);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("SearchPlans")]
        [HttpPost]
        public HttpResponseMessage SearchPlans([FromBody]searchCS searcher)
        {
            var AllPlans = Plan_Repository.GetListPlans(searcher.value, searcher.idLang, searcher.idUser );
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AllPlans);

            return response;
        }


        [Authorize(Roles = "USER")]
        //Get One Day Specific (bools control if you wanna get the info or not)
        [Route("GetDay/{numDay}/{idPlan}/{idLang}/{ExsContr}")]
        [HttpGet]
        public HttpResponseMessage GetDay(int numDay, long idPlan, string idLang, bool ExsContr)
        {
            var OneDay = DiesRepository.Get_Day(numDay, idPlan, idLang, ExsContr);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, OneDay);

            return response;
        }

        [Authorize(Roles = "USER")]
        //Get One Day Audio
        [Route("GetAudioDay/{numDay}/{idPlan}")]
        [HttpGet]
        public HttpResponseMessage GetAudioDay(int numDay, long idPlan)
        {
            var AudioDay = DiesRepository.Get_AudioDay(numDay, idPlan);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AudioDay);

            return response;
        }

        // Change Last day of the plan
        [Authorize(Roles = "USER")]
        [Route("ChangeLastDay")]
        [HttpPut]
        public HttpResponseMessage ChangeLastDay([FromBody]changeLastDayCS day)
        {
            var LastDay = DBChanges.ChangeLastDay(day.idUser, day.idPlan);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, LastDay);

            return response;
        }

        // Change the PlanStatus 
        [Authorize(Roles = "USER")]
        [Route("ChangePlanStatus")]
        [HttpPut]
        public HttpResponseMessage ChangePlanStatus([FromBody]changePlanStatusCS plan)
        {
            var PlanStatus = DBChanges.ChangePlanStatus(plan.idUser, plan.idPlan, plan.status, plan.date);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, PlanStatus);

            return response;
        }

        //Change the FeedBack of one day
        [Authorize(Roles = "USER")]
        [Route("DayFeedBack")]
        [HttpPut]
        public HttpResponseMessage DayFeedBack([FromBody]DayFeedBackCS day)
        {
            var FeedBack = DBChanges.PutFeedBackDay(day.idPlan, day.numDay, day.valoration, day.difficulty, day.comment, day.audioName, day.audioPath, day.audioFile);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, FeedBack);

            return response;
        }

        //Put the total Duration of one day Specific
        [Authorize(Roles = "USER")]
        [Route("PutTotalDuration")]
        [HttpPut]
        public HttpResponseMessage PutTotalDuration([FromBody]changeDayCS day)
        {
            var Duration = DBChanges.PutTotalDuration(day.idPlan, day.numDay);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Duration);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("ChangeLastEx")]
        [HttpPut]
        public HttpResponseMessage ChangeLastEx([FromBody]changeLastExCS ex)
        {
            var LastEx = DBChanges.ChangeLastEx(ex.idUser, ex.idPlan, ex.lastEx);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, LastEx);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("StartedDay")]
        [HttpPut]
        public HttpResponseMessage StartedDay([FromBody]changeDayCS day)
        {
            var Started = DBChanges.ChangeDayStatusStarted(day.idPlan, day.numDay);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Started);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("CompletedDay")]
        [HttpPut]
        public HttpResponseMessage CompletedDay([FromBody]changeDayCS day)
        {
            var Started = DBChanges.ChangeDayStatusToComplet(day.idPlan, day.numDay);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Started);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("ChangeDateTimeWatched")]
        [HttpPut]
        public HttpResponseMessage ChangeDateTime([FromBody]changeDateExCS ex)
        {
            var DateTime = DBChanges.ChangeDateTime(ex.idPlan, ex.numDay, ex.idWP, ex.idEx, ex.idProgram, ex.idWork);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, DateTime);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("ChangeDateCompleted")]
        [HttpPut]
        public HttpResponseMessage ChangeDateCompleted([FromBody]changeDateExCS ex)
        {
            var DateTime = DBChanges.ChangeDateCompleted(ex.idPlan, ex.numDay, ex.idWP, ex.idEx, ex.idProgram, ex.idWork);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, DateTime);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("CleanOneDay")]
        [HttpPut]
        public HttpResponseMessage CleanOneDay([FromBody]changeDayCS day)
        {
            var Clean = DBChanges.CleanOneDay(day.idPlan, day.numDay);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Clean);

            return response;
        }

        [Route("TimeElapsedExs")]
        [HttpGet]
        public HttpResponseMessage Time_elapsed_Between_Exs()
        {
            var Time = Controlador.Elapsed_Time_Betw_Exs();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Time);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("GetCountPlans/{idUser}/{Active}")]
        [HttpGet]
        public HttpResponseMessage GetCountPlans(long idUser, bool Active)
        {
            var Count = Controlador.GetCountPlans(idUser, Active);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Count);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("GetInfoPlan/{idPlan}")]
        [HttpGet]
        public HttpResponseMessage GetAllPlan(long idPlan)
        {
            try
            {
                var Count = Plan_Repository.GetInfoPlan(idPlan);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Count);

                return response;
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                return null;
            }

            
        }

        [Authorize(Roles = "USER")]
        [Route("putDownloadPlan")]
        [HttpPut]
        public HttpResponseMessage putDownloadPlan([FromBody]planCS plan)
        {
            var controller = Plan_Repository.putDownloadPlan(plan.idPlan, plan.idUser, plan.idLang);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, controller);

            return response;
        }

        [Route("SearchVariable/{filter}")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage searchListVariables(String filter)
        {
            var controller = VariablesRepository.searchListVariables(filter);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, controller);

            return response;
        }

        [Authorize(Roles = "USER")]
        [Route("SearchPlansLike")]
        [HttpPut]
        public HttpResponseMessage SearchPlanLike([FromBody]UserPlanCS userPlan)
        {
            var like = DBChanges.ChangePlanLike(userPlan.id_user, userPlan.id_plan);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, like);
 
            return response;
        }


        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

    }
}