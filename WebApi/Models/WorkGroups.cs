//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class WorkGroups
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WorkGroups()
        {
            this.Programs_WorkGroups_Works = new HashSet<Programs_WorkGroups_Works>();
            this.WorkGroups_TXT = new HashSet<WorkGroups_TXT>();
            this.WorkGroups_x_Program_x_Client = new HashSet<WorkGroups_x_Program_x_Client>();
            this.Works_x_WorkGroup = new HashSet<Works_x_WorkGroup>();
        }
    
        public int Id_WorkGroup { get; set; }
        public Nullable<int> Created_by { get; set; }
        public string Id_AccessLevel { get; set; }
        public Nullable<bool> IsMaster { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Programs_WorkGroups_Works> Programs_WorkGroups_Works { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WorkGroups_TXT> WorkGroups_TXT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WorkGroups_x_Program_x_Client> WorkGroups_x_Program_x_Client { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Works_x_WorkGroup> Works_x_WorkGroup { get; set; }
    }
}
