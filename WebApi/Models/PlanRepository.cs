﻿using System;
using System.Collections.Generic;
using System.Linq;
using RCPT.AccesoDatos;
using System.Web;
using System.Globalization;
using WebApi.ControllersDTO;

namespace WebApi.Models
{
    public class Plan_Repository
    {
        //Connexió BBDD
        private static Planendo_Test_Entities db = Controlador.db;
        //bool PlansControlador: -True: Obtindrem només els PLANS ACTIUS. -False: Obtindrem PLANS ACTIUS/INACTIUS.
        //bool DaysControlador: -true: Voldrem la informació dels dies. -False: No voldrem la informació dels dies
        //bool ExControlador: -True: voldrem la informació dels exercicis. -False: No voldrem la informació dels exercicis

        private const int statusCompleted = 1;
        private const int statusStarted = 2;
        private const int privPlanAcces = 1;
        private const int restrictedPlanAcces = 2;
        private const int publicPlanAcces = 3;
        private const int IdPlanStatusReceived = 2;

        public static List<PlanDTO> GetPlans(long idUser, string idLang, string gender, bool PlansControlador, bool DaysControlador, bool ExsControlador)
        {
            try
            {
                List<Plans> lp = new List<Plans>();
                //Si PlansControlador es true obtindrem els PLANS actius només, sinó obtindrem tots els PLANS inactius-
                if (PlansControlador) { lp = Get_ActivePlans_idUser(idUser); }
                //Obtenim els plans que són actius i inactius
                else { lp = Get_InactivePlans_idUser(idUser); }
                return Get_InfoPlans(lp, idLang, DaysControlador, ExsControlador);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                return null;
            }
        }

        //We get ONE PLAN searched by idPlan and idUser
        public static PlanDTO GetPlan(long idUser, long idPlan, string idLang, string gender, bool DaysControlador, bool ExsControlador)
        {
            try
            {
                Plans PlanB = db.Plans.Where(x => x.Id_User == idUser && x.Id_Plan == idPlan).FirstOrDefault();
                return Get_Info_Plan(PlanB, idLang, DaysControlador, ExsControlador);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                return null;
            }
        }

        //Get all plans inactive or active idc
        public static List<PlanDTO> GetAllPlans(int idUser, string idLang, string gender, bool DaysControlador, bool ExsControlador)
        {
            try
            {
                //Agafem tots els plans actius o inactius d'un usuari
                List<Plans> lp = Get_AllPlans_idUser(idUser);
                return Get_InfoPlans(lp, idLang, DaysControlador, ExsControlador);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                return null;
            }
        }

        //We get a list of PLANS that are ACTIVE from PLANSTATUS -> ACTIVESTATUS = true (Plans Active)
        public static List<Plans> Get_ActivePlans_idUser(long? idUser)
        {
            return db.Plans.Where(x => x.Id_User == idUser && x.PlanStatus.Active_Status == true).OrderByDescending(x => x.Last_Day).ToList();
        }

        //Get a list of PLANS that are inactive cancelled or another thing from PLANSTATUS -> ACTIVESTATUS = false (Plans Inactive)
        public static List<Plans> Get_InactivePlans_idUser(long? idUser)
        {
            return db.Plans.Where(x => x.Id_User == idUser && x.PlanStatus.Active_Status == false).OrderByDescending(x => x.Last_Day).ToList();
        }

        //We get a list of PLANS by idUser (ACTIVE and INACTIVE PLANS, all from 
        public static List<Plans> Get_AllPlans_idUser(long idUser)
        {
            return db.Plans.Where(x => x.Id_User == idUser).ToList();
        }

        //We get all the information of plans
        public static List<PlanDTO> Get_InfoPlans(List<Plans> lp, string idLang, bool DaysControlador, bool ExsControlador)
        {
            //List<Stored_Exercises_x_Plan> lsep = new List<Stored_Exercises_x_Plan>();
            List<PlanDTO> plans = new List<PlanDTO>();
            if (lp != null)
            {
                try
                {
                    foreach (Plans pl in lp)
                    {
                        PlanDTO p = Get_Info_Plan(pl, idLang, DaysControlador, ExsControlador);
                        if (p != null)
                        {
                            //We add a new Plan to our list of Plans 
                            plans.Add(p);
                        }
                    }
                }
                catch (Exception e)
                {
                    string s = e.Message.ToString();
                    return null;
                }
            }
            return plans;
        }

        //Get Info of Plan 
        public static PlanDTO Get_Info_Plan(Plans pl, string idLang, bool DaysControlador, bool ExsControlador)
        {
            try
            {
                PlanDTO p = new PlanDTO(pl);
                p.IdPlanStatus = db.Plans.Where(x => x.Id_Plan == pl.Id_Plan).Select(x => x.Id_PlanStatus).FirstOrDefault().Value;
                p.imgSpecialist = db.User.Where(x => x.Id_User == p.IdSpecialist).Select(x => x.Photo).FirstOrDefault();
                if (p.imgSpecialist != null) { p.imgSpecialist = p.imgSpecialist.Trim(); };
                p.imgProgramType = db.ProgramTypes.Where(x => x.Id_ProgramType == p.IdProgramType).Select(x => x.Icon_Link).FirstOrDefault();
                if (p.imgProgramType != null) { p.imgProgramType = p.imgProgramType.Trim(); };
                p.imgStatus = db.PlanStatus.Where(x => x.Id_PlanStatus == p.IdPlanStatus).Select(x => x.Icon).FirstOrDefault();
                if (p.imgStatus != null) { p.imgStatus = p.imgStatus.Trim(); };
                p.imgCompany = db.DesignTemplates.Where(x => x.Id_Company == p.IdCompany).Select(x => x.FileName).FirstOrDefault();
                if (p.imgCompany != null) { p.imgCompany = p.imgCompany.Trim(); };
                p.TypeProgramName = db.ProgramTypes_TXT.Where(x => x.Id_ProgramType == p.IdProgramType && x.Id_Lang == p.IdLang).Select(x => x.Name).FirstOrDefault();
                p.CompanyName = db.Companies.Where(x => x.Id_Company == p.IdCompany).Select(x => x.Company_Tradename).FirstOrDefault();

                p.linkSpecialist = db.User.Where(x => x.Id_User == p.IdSpecialist).Select(x => x.Link_Video_Profile).FirstOrDefault();
                p.idPlanMaster = db.Plans.Where(x => x.Id_Plan == pl.Id_Plan).Select(x => x.Id_Plan_Master).FirstOrDefault();

                p.progressStatus = GetProgressStatusPlan(p);

                //List<Stored_Exercises_x_Plan> listEx = ExerciseRepository.getListSExPlan(p.IdPlan);
                //p.lex = ExerciseRepository.GetAllExofPlan(listEx, p.IdLang);



                //We get the StatusPlan by id_PlanStatus and id_Language
                if (p.IdPlanStatus == 7)
                {
                    p.PlanStatus = db.Labels_TXT.Where(x => x.Id_Lang.Equals(idLang) && x.Id_Label.Equals("lbl_start")).Select(z => z.Name).FirstOrDefault().Trim();
                }
                else
                {
                    p.PlanStatus = db.PlanStatus_TXT.Where(x => x.Id_PlanStatus == p.IdPlanStatus && x.Id_Lang == idLang).Select(x => x.Name).First().Trim();
                }
                //Total_Time_Spent per plan
                TimeSpan? tm = db.Plans.Where(x => x.Id_Plan == p.IdPlan).Select(x => x.Total_Time_Spent).FirstOrDefault();
                if (tm == null)
                {
                    p.Total_Time_Spent = new TimeSpan(0, 0, 0).ToString(@"hh\:mm\:ss");
                }
                else
                {
                    p.Total_Time_Spent = tm.Value.ToString(@"hh\:mm\:ss");
                }
                //Si DaysControlador es true obtenim el llistat de dies.
                if (DaysControlador)
                {
                    p.ldays = DiesRepository.GetListDays(p, idLang, ExsControlador);
                    p.Progression = GetProgression(p, idLang);
                    p.ProgressionText = GetProgressionText(p.Progression, idLang);
                    p.ProgressionImg = GetProgressionImg(p.Progression, idLang);
                    //p.TuPlan = GetTextofYourPlan(p, idLang);
                    p.TimeSpent = p.Total_Time_Spent;
                    //p.TimeSpent = GetTimeSpent(p.ldays);
                }
                //Si es false no obtenim el llistat de dies
                else { p.ldays = null; }
                //getListSExPlan(p.IdPlan);

                //Return the Plan
                return p;
            }
            catch (Exception e)
            {
                string s = e.Message;
                return null;
            }
        }

        //Get Progression 
        public static double GetProgression(PlanDTO p, string idLang)
        {
            double dies = -1;
            if (p.ActualStart != null && p.ActualEnd != null && p.LastDay != null)
            {
                TimeSpan? tmp = p.LastDay - p.ActualStart;
                double diesCompletats = 0;

                foreach (var day in p.ldays)
                {
                    if (day.status_id != null)
                    {
                        if (day.status_id == statusCompleted)
                        {
                            diesCompletats = diesCompletats + 1;
                        }
                    }
                }
                //Si tenim algun dia completat fem l'operació
                if (diesCompletats != 0)
                {
                    dies = ((tmp.Value.Days / diesCompletats) - 1) * 100;
                }
                //Sino mirem a veure si ha començat algun dia o no
                else if (p.ldays[0].status_id != null)
                {
                    if (dies == 0 && p.ldays[0].status_id == statusStarted || Double.IsNaN(dies) && p.ldays[0].status_id == statusStarted || diesCompletats == 0 && p.ldays[0].status_id == statusStarted)
                    {
                        dies = 0;
                    }
                    else if (dies == 0 && p.ldays[0].status_id == statusCompleted || Double.IsNaN(dies) && p.ldays[0].status_id == statusCompleted)
                    {
                        dies = 0;
                    }
                    else if (Double.IsNaN(dies))
                    {
                        dies = -1;
                    }
                }

            }
            return dies;
        }

        //GET progression text in idLang
        public static string GetProgressionText(double progression, string idLang)
        {
            int exc = Int32.Parse(db.Variables.Where(x => x.Id_Var.Trim().Equals("PROGEXCELLENT")).Select(z => z.Value).FirstOrDefault());
            int good = Int32.Parse(db.Variables.Where(x => x.Id_Var.Trim().Equals("PROGGOOD")).Select(z => z.Value).FirstOrDefault());
            int moderate = Int32.Parse(db.Variables.Where(x => x.Id_Var.Trim().Equals("PROGMODERATE")).Select(z => z.Value).FirstOrDefault());
            int slow = Int32.Parse(db.Variables.Where(x => x.Id_Var.Trim().Equals("PROGSLOW")).Select(z => z.Value).FirstOrDefault());

            if (progression == -1) { return db.Labels_TXT.Where(x => x.Id_Lang.Equals(idLang)).Where(x => x.Id_Label.Equals("lbl_nostart")).Select(z => z.Name).FirstOrDefault().Trim(); }
            else if (progression != -1 && progression <= exc) { return db.Labels_TXT.Where(x => x.Id_Lang.Equals(idLang)).Where(x => x.Id_Label.Equals("lbl_excelent")).Select(z => z.Name).FirstOrDefault().Trim(); }
            else if (progression > exc && progression <= good) { return db.Labels_TXT.Where(x => x.Id_Lang.Equals(idLang)).Where(x => x.Id_Label.Equals("lbl_good")).Select(z => z.Name).FirstOrDefault().Trim(); }
            else if (progression > good && progression <= moderate) { return db.Labels_TXT.Where(x => x.Id_Lang.Equals(idLang)).Where(x => x.Id_Label.Equals("lbl_moderate")).Select(z => z.Name).FirstOrDefault().Trim(); }
            else if (progression > slow) { return db.Labels_TXT.Where(x => x.Id_Lang.Equals(idLang)).Where(x => x.Id_Label.Equals("lbl_slow")).Select(z => z.Name).FirstOrDefault().Trim(); }
            else { return db.Labels_TXT.Where(x => x.Id_Lang.Equals(idLang)).Where(x => x.Id_Label.Equals("lbl_nostart")).Select(z => z.Name).FirstOrDefault().Trim(); }
        }

        //Get progression string of img
        public static string GetProgressionImg(double progression, string idLang)
        {
            int exc = Int32.Parse(db.Variables.Where(x => x.Id_Var.Trim().Equals("PROGEXCELLENT")).Select(z => z.Value).FirstOrDefault());
            int good = Int32.Parse(db.Variables.Where(x => x.Id_Var.Trim().Equals("PROGGOOD")).Select(z => z.Value).FirstOrDefault());
            int moderate = Int32.Parse(db.Variables.Where(x => x.Id_Var.Trim().Equals("PROGMODERATE")).Select(z => z.Value).FirstOrDefault());
            int slow = Int32.Parse(db.Variables.Where(x => x.Id_Var.Trim().Equals("PROGSLOW")).Select(z => z.Value).FirstOrDefault());

            if (progression == -1) { return "Comb_NoStart"; }
            else if (progression <= exc) { return "Comb_Excellent"; }
            else if (progression > exc && progression <= good) { return "Comb_Good"; }
            else if (progression > good && progression <= moderate) { return "Comb_Moderate"; }
            else if (progression > slow) { return "Comb_TooSlow"; }
            else { return "Comb_NoStart"; }
        }

        //Get text day (completed day) with specific language (ex: The Date, Lunes 20 Enero)
        public static string GetTextDay(DateTime date, string idLang)
        {
            var s = "";
            var l = "";
            if (idLang == "ESP") { l = "es-ES"; }
            else if (idLang == "CAT") { l = "ca-ES"; ; }
            else if (idLang == "ENG") { l = "en-EN"; ; }
            if (date != null)
            {
                CultureInfo culture = new CultureInfo(l);
                s = date.ToString("dddd", culture) + " " + date.Day + " " + date.ToString("MMM", culture);
            }
            return s;
        }

        //Get all time spent in this plan
        public static string GetTimeSpent(List<DayDTO> ldays)
        {
            TimeSpan? time = new TimeSpan();
            foreach (var d in ldays)
            {
                if (d.Total_Duration != null)
                {
                    time = time + d.Total_Duration;
                }
            }
            string h, s, m;
            if (time.Value.Hours < 10)
            {
                int hh = time.Value.Hours;
                h = "0" + hh;
            }
            else
            {
                int hh = time.Value.Hours;
                h = "" + hh;
            }

            if (time.Value.Minutes < 10)
            {
                int mm = time.Value.Minutes;
                m = "0" + mm;
            }
            else
            {
                int mm = time.Value.Minutes;
                m = "" + mm;
            }

            if (time.Value.Seconds < 10)
            {
                int ss = time.Value.Seconds;
                s = "0" + ss;
            }
            else
            {
                int ss = time.Value.Seconds;
                s = "" + ss;
            }

            return string.Format("{0}:{1}:{2}", h, m, s);
        }

        //Get type of plan enabled
        public static List<TypeProgramCS> GetTypeProgram(string idL)
        {
            try
            {
                List<ProgramTypes> DBprograms = db.ProgramTypes.Where(x => x.Enabled == true).ToList();
                List<TypeProgramCS> programs = new List<TypeProgramCS>();

                foreach (ProgramTypes p in DBprograms)
                {
                    TypeProgramCS pro = new TypeProgramCS(p.Id_ProgramType, p.Icon_Link,
                        p.ProgramTypes_TXT.Where(x => x.Id_Lang.Equals(idL)).Select(x => x.Name).FirstOrDefault(),
                        p.Value);
                    programs.Add(pro);
                }
                return programs;
            }
            catch (Exception e)
            {
                string s = e.Message.ToString();
                return null;
            }

        }

        //Get % progress status of plan (send, complete, cancel..)
        public static int GetProgressStatusPlan(PlanDTO p)
        {
            try
            {
                List<DayDTO> dies = new List<DayDTO>();
                double preProgress = 0;
                //We get all information from Plan object
                for (int i = 0; i < p.DaysDuration; i++)
                {
                    DayDTO d;
                    //El Stored_Plans_x_Day d'on treurem la informació del nostra dia
                    Stored_Plans_x_Day Stored_day = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == p.IdPlan && x.Id_Day_Plan_Num == (i + 1)).FirstOrDefault();
                    if (Stored_day != null)
                    {
                        d = new DayDTO();
                        d.status_id = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == Stored_day.Id_Plan && x.Id_Day_Plan_Num == Stored_day.Id_Day_Plan_Num).Select(x => x.Status_Id).FirstOrDefault();
                        d.Status = db.Plan_Day_Status.Where(x => x.Id_Status == d.status_id).Select(x => x.Name).FirstOrDefault();
                    }
                    else
                        d = new DayDTO(p.IdPlan, (i + 1));

                    if (d.Status != null)
                    {
                        if (d.status_id == statusCompleted)
                        {
                            preProgress = preProgress + 1;
                        }
                        else if (d.status_id == statusStarted)
                        {
                            preProgress = preProgress + 0.5;
                        }
                    }
                }
                if (preProgress == 0)
                {
                    return 0;
                }
                else
                {
                    double final = (preProgress * 100) / p.DaysDuration;
                    return Convert.ToInt32(final);
                }

            }
            catch (Exception e)
            {
                string g = e.Message.ToString();
                return 0;
            }
        }

        public static PlanDTO GetInfoPlan(long idPlan)
        {
            try
            {
                Plans plan = db.Plans.Where(x => x.Id_Plan == idPlan).FirstOrDefault();
                PlanDTO pDTO = new PlanDTO(plan);
                List<Stored_Exercises_x_Plan> listEx = ExerciseRepository.getListSExPlan(pDTO.IdPlan);
                pDTO.lex = ExerciseRepository.GetAllExofPlan(listEx, pDTO.IdLang);
                List<Tags_x_Plan> listTags = TagsRepository.getListTagsxPlan(pDTO.IdPlan);
                pDTO.tagList = TagsRepository.getListTagsDTO(listTags);
                //pDTO.tagList = db.Tags_x_Plan.Where(x => x.Id_Plan == idPlan).ToList();
                pDTO.SpecialistVerificateUniv = db.ProfesionalDates.Where(x => x.Id_User == pDTO.IdSpecialist).Select(x => x.VerificateUniv).FirstOrDefault();
                pDTO.linkSpecialist = db.User.Where(x => x.Id_User == pDTO.IdSpecialist).Select(x => x.Link_Video_Profile).FirstOrDefault();
                return pDTO;
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                return null;
            }
        }

        public static List<PlanDTO> GetListPlans(string value, string idLang, long? idUser)
        {
            try
            {
                string[] separators = new string[] { ",", ".", "!", "\'", " ", "\'s" };
                List<Plans> lp = new List<Plans>();
                long valueNumeric;

                bool isNumeric = Int64.TryParse(value, out valueNumeric);
                long? idSpecialist = db.User.Where(x => x.Name.Equals(value)).Select(x => x.Id_User).FirstOrDefault();

                if (isNumeric)
                {
                    lp.AddRange(db.Plans.Where(x => x.Id_Plan == valueNumeric && x.Id_PlanAccess == restrictedPlanAcces && x.Active == true).ToList());
                }
                else if (value != null && value != "")
                {
                    foreach (string word in value.Split(separators, StringSplitOptions.RemoveEmptyEntries))
                        lp.AddRange(db.Plans.Where(x => x.Plan_Name_Stored.Contains(word) && x.Id_PlanAccess == publicPlanAcces && x.Active == true).ToList().Distinct().ToList());

                    foreach (string word in value.Split(separators, StringSplitOptions.RemoveEmptyEntries))
                        lp.AddRange(db.Plans.Where(x => x.Specialist_Name_Stored.Contains(word) && x.Id_PlanAccess == publicPlanAcces && x.Active == true).ToList().Distinct().ToList());

                    foreach (string word in value.Split(separators, StringSplitOptions.RemoveEmptyEntries))
                        lp.AddRange(db.Plans.Where(x => x.Company_TradeName_Stored.Contains(word) && x.Id_PlanAccess == publicPlanAcces && x.Active == true).ToList().Distinct().ToList());

                    foreach (string word in value.Split(separators, StringSplitOptions.RemoveEmptyEntries))
                    {
                        List<long?> idPlans = db.Tags_x_Plan.Where(x => x.Tag_Name.Contains(word)).Select(x => x.Id_Plan).ToList().Distinct().ToList();
                        foreach (long? idPlan in idPlans)
                            lp.Add(db.Plans.Where(x => x.Id_Plan == idPlan && x.Active == true).FirstOrDefault());
                    }
                        
                    lp = lp.Distinct().ToList();
                }
                else
                {
                    lp.AddRange(db.Plans.Where(x => x.Id_PlanAccess == publicPlanAcces && x.Active == true).ToList().Take(30).ToList());
                }

                List<PlanDTO> listPlans = new List<PlanDTO>();
                foreach (Plans p in lp)
                {
                    PlanDTO plan = new PlanDTO(p);
                    plan.SpecialistName = db.User.Where(x => x.Id_User == plan.IdSpecialist).Select(x => x.Name).FirstOrDefault();
                    plan.imgProgramType = db.ProgramTypes.Where(x => x.Id_ProgramType == p.Id_ProgramType).Select(x => x.Icon_Link).FirstOrDefault();
                    plan.imgSpecialist = db.User.Where(x => x.Id_User == plan.IdSpecialist).Select(x => x.Photo).FirstOrDefault();
                    plan.TypeProgramName = db.ProgramTypes_TXT.Where(x => x.Id_ProgramType == plan.IdProgramType && x.Id_Lang == idLang).Select(x => x.Name).FirstOrDefault();
                    plan.SpecialistVerificateUniv = db.ProfesionalDates.Where(x => x.Id_User == plan.IdSpecialist).Select(x => x.VerificateUniv).FirstOrDefault();
                    plan.like = db.Plans_Liked.Where(x => x.Id_User == idUser && x.Id_Plan == plan.IdPlan).Select(x => x.Liked).FirstOrDefault(); // new
                    if (plan.SpecialistName != null) { plan.SpecialistName = plan.SpecialistName.Trim(); }
                    if (plan.TypeProgramName != null) { plan.TypeProgramName = plan.TypeProgramName.Trim(); }
                    if (plan.imgProgramType != null) { plan.imgProgramType = plan.imgProgramType.Trim(); }
                    if (plan.imgSpecialist != null) { plan.imgSpecialist = plan.imgSpecialist.Trim(); }
                    listPlans.Add(plan);
                }
                return listPlans;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public static bool putDownloadPlan(long idPlan, long idUser, string idLang)
        {
            try
            {
                Plans oldPlan = db.Plans.Where(x => x.Id_Plan == idPlan).FirstOrDefault();
                if (oldPlan.Total_Downloads != null) oldPlan.Total_Downloads = oldPlan.Total_Downloads + 1;
                else oldPlan.Total_Downloads = 1;
                Plans newPlan = new Plans();
                newPlan.Id_Plan = (long)db.GetUniqueId().FirstOrDefault();
                newPlan.Id_Plan_Master = oldPlan.Id_Plan;
                newPlan.Id_User = idUser;
                newPlan.User_Name_Stored = db.User.Where(x => x.Id_User == idUser).Select(x => x.Surname).FirstOrDefault() + ", " + db.User.Where(x => x.Id_User == idUser).Select(x => x.Name).FirstOrDefault();
                newPlan.Id_PlanStatus = IdPlanStatusReceived;
                newPlan.Id_PlanAccess = privPlanAcces;
                newPlan.Created_Datetime = DateTime.Now;
                newPlan.Forecast_Start_Date = DateTime.Today;
                newPlan.Forecast_End_Date = DateTime.Today.AddDays(Convert.ToDouble(oldPlan.NumDays_Duration));
                newPlan.Active = true;
                newPlan.InfoESpecialist = oldPlan.InfoESpecialist;
                newPlan.Id_Plan_Master = idPlan;

                newPlan.Plan_Name_Stored = oldPlan.Plan_Name_Stored;
                newPlan.Id_Specialist = oldPlan.Id_Specialist;
                newPlan.Specialist_Name_Stored = oldPlan.Specialist_Name_Stored;
                newPlan.NumDays_Duration = oldPlan.NumDays_Duration;
                newPlan.Id_Company = oldPlan.Id_Company;
                newPlan.Id_Lang = oldPlan.Id_Lang;
                newPlan.Id_Level = oldPlan.Id_Level;
                newPlan.Id_Level_series = oldPlan.Id_Level_series;
                //newPlan.Id_Picture = oldPlan.Id_Picture;
                newPlan.Id_ProgramType = oldPlan.Id_ProgramType;
                newPlan.Id_Progression = oldPlan.Id_Progression;
                newPlan.Sequential = oldPlan.Sequential;
                newPlan.Specialist_Name_Stored = oldPlan.Specialist_Name_Stored;
                newPlan.Company_TradeName_Stored = oldPlan.Company_TradeName_Stored;
                newPlan.Id_ProgramAccess = oldPlan.Id_ProgramAccess;
                newPlan.Price = oldPlan.Price;
                newPlan.free = oldPlan.free;

                List<Stored_Exercises_x_Plan> listExs = addExercisesNewPlan(oldPlan.Id_Plan, newPlan.Id_Plan, idLang);
                db.Plans.Add(newPlan);

                db.Stored_Exercises_x_Plan.AddRange(listExs);
                
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.Write(e.InnerException.InnerException.Message);
                return false;
            }
        }

        public static List<Stored_Exercises_x_Plan> addExercisesNewPlan(long? idPlanMaster, long? idPlan, string idLang)
        {
            try
            {
                List<Stored_Exercises_x_Plan> listExs = new List<Stored_Exercises_x_Plan>();
                foreach (Stored_Exercises_x_Plan ex in db.Stored_Exercises_x_Plan.Where(x=> x.Id_Plan == idPlanMaster).ToList())
                {
                    Stored_Exercises_x_Plan newEx = new Stored_Exercises_x_Plan();
                    newEx.order_id = ex.order_id;
                    newEx.Id_Plan = (long)idPlan;
                    newEx.Id_Exercise = ex.Id_Exercise;
                    newEx.Id_Work = ex.Id_Work;
                    newEx.Id_WorkPhase = ex.Id_WorkPhase;
                    newEx.Id_Program = ex.Id_Program;
                    newEx.Id_Day_Plan_Num = ex.Id_Day_Plan_Num;

                    newEx.Id_Picture = ex.Id_Picture;
                    newEx.Id_Unit = ex.Id_Unit;
                    newEx.Link_Video_FemaleAdult_Stored = ex.Link_Video_FemaleAdult_Stored;
                    newEx.Link_Video_MaleAdult_Stored = ex.Link_Video_MaleAdult_Stored;
                    newEx.Picture_Repository = ex.Picture_Repository;
                    newEx.Program_Name_Stored = db.Programs_TXT.Where(x => x.Id_Program == ex.Id_Program && x.Id_Lang.Equals(idLang)).Select(x => x.Name).FirstOrDefault(); 
                    newEx.Series_Num_Stored = ex.Series_Num_Stored;
                    newEx.Unit_Num_Stored = ex.Unit_Num_Stored;
                    newEx.Weight_Num_Stored = ex.Weight_Num_Stored;
                    newEx.WorkPhase_Name_Stored = ex.WorkPhase_Name_Stored;
                    newEx.Work_Name_Stored = ex.Work_Name_Stored;
                    newEx.Exercise_Name_Stored = db.Exercise_TXT.Where(x => x.Id_Exercise == newEx.Id_Exercise && x.Id_Lang.Equals(idLang)).Select(x => x.Name).FirstOrDefault();
                    newEx.Exercise_NumDays_Duration = ex.Exercise_NumDays_Duration;
                    newEx.Exercise_NumDay_Start = ex.Exercise_NumDay_Start;

                    listExs.Add(newEx);
                }

                return listExs;
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
                return null;
            }
        }
    }
}