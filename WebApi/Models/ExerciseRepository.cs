﻿using RCPT.AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.ControllersDTO;

namespace WebApi.Models
{
    public class ExerciseRepository
    {
        //Connexió BBDD
        private static Planendo_Test_Entities db = Controlador.db;

        //list of All Stored_Exercise_x_Plan by id_Plan
        public static List<Stored_Exercises_x_Plan> getListSExPlan(long idPlan)
        {
            try
            {
                return db.Stored_Exercises_x_Plan.Where(x => x.Id_Plan == idPlan).OrderBy(x => x.order_id).ToList();
            }
            catch (Exception e)
            {
                string s = e.Message.ToString();
                return null;
            }
        }

        //Save all Stored Exercise x Plan CS in a list an return it 
        public static List<Stored_Ex_Plan_DTO> GetAllExofPlan(List<Stored_Exercises_x_Plan> lsep, string idLang)
        {
            List<Stored_Ex_Plan_DTO> sCS = new List<Stored_Ex_Plan_DTO>();
            try
            {
                foreach (Stored_Exercises_x_Plan s in lsep)
                {
                    Stored_Ex_Plan_DTO sp = new Stored_Ex_Plan_DTO(s, idLang);

                    if (sp != null)
                    {
                        sCS.Add(sp);
                    }
                }
                return sCS;

            }
            catch (Exception e)
            {
                string s = e.Message.ToString();
                return null;
            }
        }

        //Return the info of Exercise ( We pass the info of Ex_x_Plan to Ex_Plan_CS) with idLanguage too
        public static Stored_Ex_Plan_DTO GetInfoEx(Stored_Exercises_x_Plan sexp, int numDay, string idLang)
        {
            Stored_Ex_Plan_DTO ex;
            try
            {
                ex = new Stored_Ex_Plan_DTO(sexp);

                ex.orderNum = db.WorkPhases.Where(x => x.Id_WorkPhase == ex.idWorkPhase).Select(x => x.OrderNum).SingleOrDefault();
                if (ex.LinkVideoMaleAdult == null && ex.LinkVideoFemaleAdult == null && ex.pictureFemale == null && ex.pictureMale == null)
                {
                    ex.LinkVideoMaleAdult = db.Exercises.Where(x => x.Id_Exercise == ex.idEx).Select(z => z.Link_Picture_MaleAdult).SingleOrDefault().Trim();
                    ex.LinkVideoFemaleAdult = db.Exercises.Where(x => x.Id_Exercise == ex.idEx).Select(z => z.Link_Video_FemaleAdult).SingleOrDefault().Trim();
                    ex.pictureFemale = db.Exercises.Where(x => x.Id_Exercise == ex.idEx).Select(z => z.Link_Picture_FemaleAdult).SingleOrDefault().Trim();
                    ex.pictureMale = db.Exercises.Where(x => x.Id_Exercise == ex.idEx).Select(z => z.Link_Picture_MaleAdult).SingleOrDefault().Trim();
                }
                ex.WorkPhaseName = db.WorkPhases_TXT.Where(x => x.Id_WorkPhase == ex.idWorkPhase && x.Id_Lang == idLang).Select(z => z.Name).FirstOrDefault().Trim();
                ex.UnitName = db.Units_TXT.Where(x => x.Id_Lang == idLang && ex.idUnit == x.Id_Unit).Select(z => z.Name).FirstOrDefault().Trim();
                ex.idProgression = db.Plans.Where(x => x.Id_Plan == ex.idPlan).Select(x => x.Id_Progression).FirstOrDefault();
                ex.idLevel = db.Plans.Where(x => x.Id_Plan == ex.idPlan).Select(x => x.Id_Level).FirstOrDefault();
                ex.idLevel_series = db.Plans.Where(x => x.Id_Plan == ex.idPlan).Select(x => x.Id_Level_series).FirstOrDefault();

                //Primer obtenim el valor base 
                ex.UnitNum = getBaseUnitNumber(ex);
                ex.SeriesNum = getBaseSeriesNumber(ex);
                //Ara li apliquem segons en el moment de la progressió en el que esta
                ex.UnitNum = getUnitNumber(ex, numDay);
                //Ara amb les series
                ex.SeriesNum = getSeriesNumber(ex, numDay);

                //Obtenir el Status del Exercise a través del idPlan que volguem (idPlanMaster||idPlan)
                /*if (idPlanMaster != null && idPlanMaster != 0) { ex = getStatusExerciseByIdPlan(ex, numDay, idPlanMaster); }
                else { ex = getStatusExerciseByIdPlan(ex, numDay, ex.idPlan); }*/
                ex = getStatusExerciseByIdPlan(ex, numDay, ex.idPlan);

                return ex;
            }
            catch (Exception e)
            {
                String s = e.Message.ToString();
                return null;
            }
        }

        //Get the base unit number of the exercise
        public static int? getBaseSeriesNumber(Stored_Ex_Plan_DTO ex)
        {
            int sumaUnits = Convert.ToInt32(db.Levels.Where(x => x.Id_Level == ex.idLevel_series).Select(x => x.Factor_Series_Num).FirstOrDefault());
            return ex.SeriesNum + sumaUnits;
        }

        public static int getBaseUnitNumber(Stored_Ex_Plan_DTO ex)
        {
            int sumaUnits = Convert.ToInt32(db.Levels.Where(x => x.Id_Level == ex.idLevel).Select(x => x.Factor_Units_Num).FirstOrDefault());
            return ex.UnitNum + sumaUnits;
        }

        //Get the series number of the exercise
        public static int? getSeriesNumber(Stored_Ex_Plan_DTO ex, int numDay)
        {
            int? idProgression = ex.idProgression;
            bool? noprogress = db.Exercises.Where(x => x.Id_Exercise == ex.idEx).Select(x => x.no_progresion).FirstOrDefault();
            if (idProgression == 1 || idProgression == null || noprogress == true)
            {
                return ex.SeriesNum;
            }
            int? numSeries = ex.SeriesNum.GetValueOrDefault();
            int countDays = 0;
            int sumaSeries = Convert.ToInt32(db.Progressions.Where(x => x.Id_Progression == idProgression).Select(x => x.Factor_Series_Num).FirstOrDefault());
            int? maxSeries = db.Progressions.Where(x => x.Id_Progression == idProgression).Select(x => x.max_Series).FirstOrDefault();
            int? frencuencia = db.Progressions.Where(x => x.Id_Progression == idProgression).Select(x => x.Frecuencia).FirstOrDefault();
            for (int i = 0; i < numDay; i++)
            {
                if (countDays == frencuencia && numSeries < maxSeries)
                {
                    numSeries = numSeries + sumaSeries;
                    countDays = 1;
                }
                else if (numSeries >= maxSeries)
                {
                    return maxSeries;
                }
                else
                {
                    countDays++;
                }
            }
            return numSeries;
        }

        //Get the series number of the exercise
        public static int getUnitNumber(Stored_Ex_Plan_DTO ex, int numDay)
        {
            int? idProgression = ex.idProgression;
            bool? noprogress = db.Exercises.Where(x => x.Id_Exercise == ex.idEx).Select(x => x.no_progresion).FirstOrDefault();
            if (idProgression == 1 || idProgression == null || noprogress == true)
            {
                return ex.UnitNum;
            }
            int unitNum = ex.UnitNum;
            int countDays = 0;
            int sumaUnits = Convert.ToInt32(db.Progressions.Where(x => x.Id_Progression == idProgression).Select(x => x.Factor_Units_Num).FirstOrDefault());
            int? maxUnits = db.Progressions.Where(x => x.Id_Progression == idProgression).Select(x => x.max_Units).FirstOrDefault();
            int? frencuencia = db.Progressions.Where(x => x.Id_Progression == idProgression).Select(x => x.Frecuencia).FirstOrDefault();
            for (int i = 0; i < numDay; i++)
            {
                if (countDays == frencuencia && unitNum < maxUnits)
                {
                    unitNum = unitNum + sumaUnits;
                    countDays = 1;
                }
                else if (unitNum >= maxUnits)
                {
                    return unitNum;
                }
                else
                {
                    countDays++;
                }
            }
            return unitNum;
        }

        //Return the info of Exercise ( We pass the info of Ex_x_Plan to Ex_Plan_CS) with idLanguage too
        public static Stored_Ex_Plan_DTO ExsOfPlan(Stored_Exercises_x_Plan sexp, string idLang)
        {
            try
            {
                return new Stored_Ex_Plan_DTO(sexp, idLang);
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return null;
            }
        }

        //Return all exercises of plan x day (p, d.idNumDay ,idLang, boolean); El controlador es per saber com omplirem l'informació 
        public static List<Stored_Ex_Plan_DTO> GetAllExsDay(int numDay, long? idPlan, string idLang)
        {
            try
            {
                List<Stored_Ex_Plan_DTO> Exercises = new List<Stored_Ex_Plan_DTO>();

                //Funciona amb plans antics
                IOrderedEnumerable<Stored_Exercises_x_Plan> ExsPlan = db.Stored_Exercises_x_Plan.Where(x => x.Id_Plan == idPlan && x.Exercise_NumDay_Start <= numDay && (x.Exercise_NumDay_Start + x.Exercise_NumDays_Duration) > numDay)
                        .ToList()
                        .OrderBy(x => x.WorkPhases.OrderNum)
                        .OrderBy(x => x.order_id)
                        .ThenBy(x => x.Id_Work);
                //ExsPlan.Count() == 0, hauria de retornar 3 amb el idPlan = 1353511929

                //Funciona amb plans nous, on pots escollir si la sessio es independent o amb progressio
                if (ExsPlan.Count() == 0)
                {
                    ExsPlan = db.Stored_Exercises_x_Plan.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay)
                        .ToList()
                        .OrderBy(x => x.WorkPhases.OrderNum)
                        .OrderBy(x => x.order_id)
                        .ThenBy(x => x.Id_Work);
                }

                foreach (var ex in ExsPlan)
                {
                    Stored_Ex_Plan_DTO newEx = GetInfoEx(ex, numDay, idLang);
                    if (newEx != null)
                        Exercises.Add(newEx);
                }
                //Si no son secuencials haurem de desordenarlos seguint l'estructura de workphase
                if (!db.Plans.Where(x => x.Id_Plan == idPlan).Select(x => x.Sequential).FirstOrDefault().Value)
                {
                    return getOrderExercisesNoSecuencial(Exercises, numDay, idPlan, idLang);
                }
                return Exercises;
            }
            catch (Exception e)
            {
                string s = e.Message;
                return null;
            }
        }

        //Return list of exercises with no secuencial order
        public static List<Stored_Ex_Plan_DTO> getOrderExercisesNoSecuencial(List<Stored_Ex_Plan_DTO> listExs, int numDay, long? idPlan, string idLang)
        {
            List<Stored_Ex_Plan_DTO> Exercises = new List<Stored_Ex_Plan_DTO>();
            List<Stored_Ex_Plan_DTO> arrWorkPhase6 = new List<Stored_Ex_Plan_DTO>();
            List<int> works = listExs.Where(x => x.idWorkPhase == 6).Select(x => x.idWork).ToList().Distinct().ToList();
            bool control = false;
            //For each per obtenir tots els exercicis que estiguin a treball princiapl
            foreach (Stored_Ex_Plan_DTO actualEx in listExs.Where(x => x.idWorkPhase == 6).OrderBy(x => x.idWork).ToList())
            {
                arrWorkPhase6.Add(actualEx);
            }
            //Desordenem aquests perquè no estiguin junts tots els exercicis d'una mateixa workphase
            arrWorkPhase6 = desordenarNoSecuencial(arrWorkPhase6, works);
            //Ara els agafem tots i els que siguin de 6 no
            foreach (var ex in db.Stored_Exercises_x_Plan.Where(x => x.Id_Plan == idPlan && x.Exercise_NumDay_Start <= numDay && (x.Exercise_NumDay_Start + x.Exercise_NumDays_Duration) > numDay).ToList()
                  .OrderBy(x => x.WorkPhases.OrderNum).OrderBy(x => x.order_id))
            {
                //Si es el primer cop que està el workphase de 5 afegim tots els exercicis desordenats a l'Array i després posem el control a true perquè no tonri a entrar
                if (ex.Id_WorkPhase == 6 && !control)
                {
                    Exercises.AddRange(arrWorkPhase6);
                    control = true;
                }
                //Si no es workphase = a treball principal afegim el exercici sense cap problema
                else if (ex.Id_WorkPhase != 6)
                {
                    Stored_Ex_Plan_DTO newEx = GetInfoEx(ex, numDay, idLang);
                    if (newEx != null)
                        Exercises.Add(newEx);
                }
            }
            return Exercises;
        }

        //Rebem una list de exercicis (en teoria de treball principal i totes les id_works que tenen), Retornem la List no secuencial que afegiria un de cada
        public static List<Stored_Ex_Plan_DTO> desordenarNoSecuencial(List<Stored_Ex_Plan_DTO> exs, List<int> works)
        {
            try
            {
                List<Stored_Ex_Plan_DTO> Exercises = new List<Stored_Ex_Plan_DTO>();
                while (exs.Count != 0)
                {
                    foreach (int actualWork in works)
                    {
                        Stored_Ex_Plan_DTO actualEx = exs.Where(x => x.idWork == actualWork).FirstOrDefault();
                        if (actualEx != null)
                        {
                            Exercises.Add(actualEx);
                            exs.Remove(actualEx);
                        }
                    }
                }
                return Exercises;
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return exs;
            }

        }

        public static Stored_Ex_Plan_DTO getStatusExerciseByIdPlan(Stored_Ex_Plan_DTO ex, int numDay, long? idPlan)
        {
            ex.Start_DateTime = db.Stored_Plans_Exercises_x_Day.Where(x => x.Id_Day_Plan_Num == numDay && x.Id_Plan == idPlan &&
                    x.Id_Exercise == ex.idEx && x.Id_Program == ex.idProgram & x.Id_Workphase == ex.idWorkPhase && x.Id_Work == ex.idWork)
                    .Select(z => z.Datetime_watched).FirstOrDefault();

            if (db.Stored_Plans_Exercises_x_Day.Where(x => x.Id_Day_Plan_Num == numDay && x.Id_Plan == idPlan
                && x.Id_Exercise == ex.idEx && x.Id_Program == ex.idProgram & x.Id_Workphase == ex.idWorkPhase && x.Id_Work == ex.idWork)
                .Select(z => z.Datetime_completed).FirstOrDefault() != null)
            { ex.isRealized = true; }

            else if (db.Stored_Plans_Exercises_x_Day.Where(x => x.Id_Day_Plan_Num == numDay && x.Id_Plan == idPlan
                && x.Id_Exercise == ex.idEx && x.Id_Program == ex.idProgram & x.Id_Workphase == ex.idWorkPhase && x.Id_Work == ex.idWork)
                .Select(z => z.Datetime_completed).FirstOrDefault() == null
               &&
               db.Stored_Plans_Exercises_x_Day.Where(x => x.Id_Day_Plan_Num == numDay && x.Id_Plan == idPlan
               && x.Id_Exercise == ex.idEx && x.Id_Program == ex.idProgram & x.Id_Workphase == ex.idWorkPhase && x.Id_Work == ex.idWork)
                .Select(z => z.Datetime_watched).FirstOrDefault() != null)
            { ex.isRealized = false; }

            else { ex.isRealized = null; }

            return ex;
        }

    }
}