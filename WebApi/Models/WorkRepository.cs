﻿using RCPT.AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class WorkRepository
    {
        private static rcptEntities1 db = new rcptEntities1();

        //Get all Works
        public static List<Works> getAllWorks()
        {
            List<Works> lw = db.Works.ToList();
            return lw;
        }
        //Get Work by ID Language
        public static List<Workcs> getWorksByIdL(string IdL)
        {
            List<Works> lw = new List<Works>();
            List<Workcs> lwreturn = new List<Workcs>();
            lw = db.Works.ToList();
            try
            {
                foreach (Works ws in lw)
                {
                    Workcs w = new Workcs();
                    w.IdWork = ws.Works_TXT.Where(x => x.Id_Lang == IdL).Select(x => x.Id_Work).First();
                    w.linkPictureExerciseMale = ws.Link_Picture_MaleAdult;
                    w.linkPictureExerciseFemale = ws.Link_Picture_FemaleAdult;
                    w.linkVideoExerciseFemale = ws.Link_Video_FemaleAdult;
                    w.linkVideoExerciseMale = ws.Link_Video_MaleAdult;
                    w.UnitsNum = (int)ws.Units_Num;
                    w.WeightNum = ws.Weight_Num;
                    w.CreatedBy = ws.Created_By.GetValueOrDefault();
                    w.AccesLabel = ws.Id_AccessLevel;
                    w.name = ws.Works_TXT.Where(x => x.Id_Work == w.IdWork).Select(x => x.Name).First();
                    w.observations = ws.Works_TXT.Where(x => x.Id_Work == w.IdWork).Select(x => x.Observations).First();
                    w.isMaster = ws.IsMaster.GetValueOrDefault();

                    if (w.IdWork != null || w.IdWork != -1)
                    {
                        lwreturn.Add(w);
                    }
                }
            } catch (Exception ex)
            {
                string s = ex.Message.ToString();
                return lwreturn;
            }
            if(lwreturn.Count == 0)
            {
                throw new NotImplementedException();
            } else {
                return lwreturn;
            }
            
        }
        

        //Get By ID Language and Name
        public List<Workcs> GetByName(string IdL, string name)
        {
            List<Workcs> p = new List<Workcs>();
            List<Works> wrks = new List<Works>();
            try
            {
                // wrks = db.Works_TXT.Where(x => x.Name.Contains(name)).ToList(); 
                wrks = db.Works.ToList();
                foreach (Works ws in wrks)
                {
                    Workcs w = new Workcs();
                    w.IdExercise = ws.Works_TXT.Where(x => x.Id_Lang == IdL).Select(x => x.Id_Work).First();
                    w.linkPictureExerciseMale = ws.Link_Picture_MaleAdult;
                    w.linkPictureExerciseFemale = ws.Link_Picture_FemaleAdult;
                    w.linkVideoExerciseFemale = ws.Link_Video_FemaleAdult;
                    w.linkVideoExerciseMale = ws.Link_Video_MaleAdult;
                    w.UnitsNum = (int)ws.Units_Num;
                    w.WeightNum = ws.Weight_Num;
                    w.CreatedBy = (int)ws.Created_By;
                    w.AccesLabel = ws.Id_AccessLevel;
                    w.name = ws.Works_TXT.Where(x => x.Id_Work == w.IdWork).Select(x => x.Name).First();
                    w.observations = ws.Works_TXT.Where(x => x.Id_Work == w.IdWork).Select(x => x.Observations).First();
                    w.isMaster = (bool)ws.IsMaster;

                    //c.IdEjercicio = ej.Id_Exercise;
                    //------ c.nombreEjercicio = ej.Works_TXT.Where(x => x.Id_Lang == IdL).Where(x => x.Id_Exercise == ej.Id_Exercise).
                    if (w.IdExercise != null) { p.Add(w); }

                }
                return p;
            }
            catch (Exception ex)
            {
                string s = ex.Message.ToString();
                return p;
            }
        }
    }
}