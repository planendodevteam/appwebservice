//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Levels
    {
        public int Id_Level { get; set; }
        public Nullable<int> OrderNum { get; set; }
        public Nullable<bool> Default_Level { get; set; }
        public Nullable<double> Factor_Series_Num { get; set; }
        public Nullable<double> Factor_Units_Num { get; set; }
        public int Id_units { get; set; }
    }
}
