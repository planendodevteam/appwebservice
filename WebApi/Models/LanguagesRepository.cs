﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using RCPT.AccesoDatos;

namespace WebApi.Models
{
    public class LanguagesRepository
    {
        private static Planendo_Test_Entities db = Controlador.db;
        

        public static bool SaveDB()
        {
            try { db.SaveChanges(); return true; } catch (Exception e) { e.Message.ToString(); return false; }
        }

        public static List<MenuCS> GetMenu(string idL)
        {
            string app = "APP";
            List<MenuCS> menuCS = new List<MenuCS>();
            try
            {
                List<Menu> listMenu = db.Menu.Where(x => x.Id_GUI.Equals(app) && x.Enabled == true).ToList();
                foreach (Menu m in listMenu)
                {
                    MenuCS menu = new MenuCS();
                    menu.name = m.Menu_TXT.Where(x => x.Id_Lang.Equals(idL)).Select(x => x.Name).SingleOrDefault().Trim();
                    menu.Order = m.OrderNum;
                    menu.link = m.Link.Trim();
                    menu.icono = m.Icono_Link.Trim();
                    menuCS.Add(menu);
                }
                return menuCS;
            }
            catch (Exception e)
            {
                string s = e.Message.ToString();
                return null;
            }
        }

        //Get all idLanguage and name language
        public static List<Idioma> GetAll()
        {
            List<Idioma> p = new List<Idioma>();
            List<Languages> e = new List<Languages>();
            try
            {
                e = db.Languages.ToList();
                foreach (Languages l in e)
                {
                    Idioma c = new Idioma();
                    c.IdL = l.Id_Lang.Trim();
                    c.nombre = l.Name.Trim();
                    p.Add(c);
                }
                return p;
            }
            catch (Exception ex)
            {
                string s = ex.Message.ToString();
                return p;
            }
        }

        //Get labels by array of name labels and idLanguage
        public static List<Label> GetLabels(string lbls, string idL)
        {
            string[] lbls2;
            List<Label> lbl = new List<Label>();
            lbls2 = Regex.Split(lbls, ",");
            if (lbls2 != null)
            {
                foreach (string s in lbls2)
                {
                    Label label = new Label();
                    Labels_TXT lb = new Labels_TXT();
                    try
                    {
                        lb = db.Labels_TXT.Where(x => x.Id_Label == s && x.Id_Lang == idL).First();
                    }
                    catch (Exception e)
                    {
                        string a = e.Message.ToString();
                    }
                    if (lb.Name != null)
                    {
                        label.Name = lb.Name.Trim();
                        label.idLabel = lb.Id_Label.Trim();
                        lbl.Add(label);
                    }
                }
            }
            return lbl;
        }

        //Menu x array Labels(icono name) and idLanguage
        public static List<MenuCS> MenuByidL(string lbls, string idL)
        {
            string app = "APP";
            Menu menu = new Menu();
            string[] lbls2;
            lbls2 = Regex.Split(lbls, ",");
            List<MenuCS> menuCS = new List<MenuCS>();
            foreach (string s in lbls2)
            {
                MenuCS m = new MenuCS();
                Menu_TXT lb = new Menu_TXT();
                try
                {
                    menu = db.Menu.Where(x => x.Id_GUI == app).Where(z => z.Icono_Link == s).First();
                    lb = db.Menu_TXT.Where(x => x.Id_Menu == menu.Id_Menu).Where(z => z.Id_Lang == idL).First();
                }
                catch (Exception e)
                {
                    string a = e.Message.ToString();
                    return menuCS;
                }
                if (lb != null)
                {
                    m.name = lb.Name.Trim();
                    m.Order = menu.OrderNum;
                    m.link = menu.Link.Trim();
                    m.icono = s;
                    menuCS.Add(m);
                }
            }
            return menuCS;
        }
    }
}