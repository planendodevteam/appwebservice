//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Programs_x_Client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Programs_x_Client()
        {
            this.WorkGroups_x_Program_x_Client = new HashSet<WorkGroups_x_Program_x_Client>();
        }
    
        public int Id_Treatment { get; set; }
        public int Id_Patient { get; set; }
        public int Id_Specialist { get; set; }
        public int Id_Company { get; set; }
        public System.DateTime Date_Start { get; set; }
        public System.DateTime Date_End { get; set; }
        public Nullable<System.DateTime> Date_Creation { get; set; }
    
        public virtual Clients Clients { get; set; }
        public virtual Companies Companies { get; set; }
        public virtual Programs Programs { get; set; }
        public virtual Specialists Specialists { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WorkGroups_x_Program_x_Client> WorkGroups_x_Program_x_Client { get; set; }
    }
}
