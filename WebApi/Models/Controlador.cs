﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class Controlador
    {
        //Connexió BBDD
        public static Planendo_Test_Entities db = new Planendo_Test_Entities();

        public static Planendo_Test_Entities DB()                                                                  
        {
            try
            {
                return db;
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return null;
            }
        }

        //Get Count of plans for user id and active or inactive plans
        public static int GetCountPlans(long idUser, bool Active)
        {
            int count = -1;
            try
            {
                count = db.Plans.Where(x => x.Id_User == idUser && x.PlanStatus.Active_Status == Active).ToList().Count();
                return count;
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return count;
            }
        }

        public static bool ControlBool(bool? myBool)
        {
            bool newBool = myBool ?? false;
            if (newBool)
            { return true; }
            else { return false; }
        }

        public static int ControlInt (int? myInt)
        {
            if (myInt.HasValue)
            {
                return myInt.Value;
            } else { return -1; }
        }

        public static string Elapsed_Time_Betw_Exs()
        {
            return db.Variables.Where(x => x.Id_Var.Equals("MAXELAPSEDTIME")).Select(z => z.Value.Trim()).First();
        }
    }
}