﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace WebApi.Models
{
    public class DBChanges
    {
        //Connexió BBDD
        private static Planendo_Test_Entities db = Controlador.DB();
        private static int statusCompleted = 1;
        private static int statusStarted = 2;

        public static bool SaveDB(Planendo_Test_Entities db2)
        {
            try
            {
                db2.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return false;
            }
        }

        //Change the last day of one specific plan (idUser, idPlan, LastDay)
        public static bool ChangeLastDay(long idUser, long idPlan)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            Plans p = db.Plans.Where(x => x.Id_User == idUser && x.Id_Plan == idPlan).First();
            try
            {
                p.Last_Day = DateTime.Now;
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return false;
            }
            return SaveDB(db);
        }

        //Change the last exercise_id of one specific plan (idUser, idPlan, LastidEx)
        public static bool ChangeLastEx(long idUser, long idPlan, long lastEx)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            Plans p = db.Plans.Where(x => x.Id_User == idUser && x.Id_Plan == idPlan).First();
            try
            {
                p.Last_Exercise_Id = lastEx;
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return false;
            }
            return SaveDB(db);
        }

        //Change plan status to what App says ( We can add the date of start or end)
        public static bool ChangePlanStatus(long idUser, long idPlan, int status, int Date)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            try
            {
                Plans p = db.Plans.Where(x => x.Id_Plan == idPlan && x.Id_User == idUser).First();
                p.Id_PlanStatus = status;
                if (Date == 1) { p.Actual_Start_Datetime = DateTime.Now; p.Message_viewdate = DateTime.Now; }
                else if (Date == 2) { p.Actual_End_Datetime = DateTime.Now; p.Alert_Plan_Completed = 1; }
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return false;
            }
        }

        //Put the Total_Duration time spent of one day
        public static bool PutTotalDuration(long idPlan, int numDay)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            TimeSpan? duration = new TimeSpan();
            //Agafem el dia per posar-li el temps total de la suma de tots els exercicis
            Stored_Plans_x_Day day = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).First();
            //Agafem el dia per posar-li el temps total de la suma de tots els exercicis
            Plans plan = db.Plans.Where(x => x.Id_Plan == idPlan).FirstOrDefault();

            //Agafem el llistat de dies per sumar tot el temps després i restar-li al total per saber quan temps hem invertit en aquest dia
            List<Stored_Plans_x_Day> days = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan).ToList();

            try
            {
                //for each amb tots els exercicics que estiguin dins del dia del plan
                foreach (var d in days)
                {
                    if (d.Total_Duration != null)
                    {
                        duration = duration + d.Total_Duration;
                    }
                }
                duration = plan.Total_Time_Spent - duration;
                TimeSpan s = new TimeSpan(0, 0, 0, 0);
                //Si es valor negatiu el passem a positiu
                if (duration < s) { duration = TimeSpan.FromMinutes(duration.Value.TotalMinutes * (-1)); }
                day.Total_Duration = duration;
                //plan.Total_Time_Spent = duration;
                Console.WriteLine(day.Total_Duration);
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return false;
            }
            return SaveDB(db);
        }

        //Change the feedbback of one specific day (valoration with stars and comment)
        public static bool PutFeedBackDay(long idPlan, int numDay, int valoration, int difficulty, string comment, string audioName, string audioPath, string audioFile)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            Stored_Plans_x_Day day = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).FirstOrDefault();
            Plans plan = db.Plans.Where(x => x.Id_Plan == idPlan).FirstOrDefault();
            try
            {
                day.Feedback_User_Rating_Id = valoration;
                day.Feedback_User_Dificulty_Id = difficulty;
                day.Date_Completed = DateTime.Now;
                day.Status_Id = statusCompleted; //Completed
                day.Comment = comment;

                if (day.Comment != "" && day.Comment != null) { plan.Alert_Session_txt = 1; }
                if (day.Feedback_User_Rating_Id == 1) { plan.Alert_Session_Unhappy = 1; }

                if (audioName != null)
                {
                    Stored_Recording recorder = new Stored_Recording();
                    recorder.Id_Plan = idPlan;
                    recorder.Id_Day_Plan_Num = numDay;
                    recorder.AudioFile = audioFile;
                    recorder.AudioPath = audioPath;
                    recorder.AudioName = audioName;

                    db.Stored_Recording.Add(recorder);

                    plan.Alert_Session_voice = 1;
                }

                return SaveDB(db);
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return false;
            }
        }

        //Change day status to Started
        public static bool ChangeDayStatusStarted(long idPlan, int numDay)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            try
            {
                Stored_Plans_x_Day day = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).First();
                day.Status_Id = statusStarted;
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                try
                {
                    Stored_Plans_x_Day day = new Stored_Plans_x_Day();
                    day.Id_Plan = idPlan;
                    day.Id_Day_Plan_Num = numDay;
                    day.Status_Id = statusStarted;
                    db.Stored_Plans_x_Day.Add(day);
                }
                catch (Exception ex)
                {
                    string s = ex.Message.ToString();
                    return false;
                }
            }
            return SaveDB(db);
        }

        //Change day status to Completed
        public static bool ChangeDayStatusToComplet(long idPlan, int numDay)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            Stored_Plans_x_Day day = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).FirstOrDefault();
            try
            {
                day.Date_Completed = DateTime.Now;
                day.Status_Id = statusCompleted;
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return false;
            }
        }

        //Change the DateTime of DateStarted of one exercise in specific day
        public static bool ChangeDateTime(long idPlan, int numDay, int idWP, long idEx, long idProgram, int idWork)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            try
            {
                Stored_Plans_Exercises_x_Day spex = db.Stored_Plans_Exercises_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Workphase == idWP
                    && x.Id_Day_Plan_Num == numDay && x.Id_Exercise == idEx && x.Id_Program == idProgram && x.Id_Work == idWork).First();
                spex.Datetime_watched = DateTime.Now;

            }
            catch (Exception ex)
            {
                string s = ex.Message.ToString();
                try
                {
                    Stored_Plans_Exercises_x_Day spex = new Stored_Plans_Exercises_x_Day();
                    spex.Id_Plan = idPlan;
                    spex.Id_Exercise = idEx;
                    spex.Id_Workphase = idWP;
                    spex.Id_Day_Plan_Num = numDay;
                    spex.Datetime_watched = DateTime.Now;
                    spex.Id_Work = idWork;
                    spex.Id_Program = idProgram;
                    db.Stored_Plans_Exercises_x_Day.Add(spex);
                }
                catch (Exception e)
                {
                    string w = e.Message.ToString();
                    return false;
                }
            }
            return SaveDB(db);
        }

        //Change the DateTime of DateCompleted of one exercise in specific day
        public static bool ChangeDateCompleted(long idPlan, int numDay, int idWP, long idEx, long idProgram, int idWork)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            try
            {
                Stored_Plans_Exercises_x_Day spex = db.Stored_Plans_Exercises_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Workphase == idWP && x.Id_Day_Plan_Num == numDay &&
                x.Id_Exercise == idEx && x.Id_Program == idProgram && x.Id_Work == idWork).First();
                spex.Datetime_completed = DateTime.Now;
                Plans plan = db.Plans.Where(x => x.Id_Plan == idPlan).FirstOrDefault();
                DateTime CompletedTime = (DateTime)spex.Datetime_completed;
                DateTime StartTime = (DateTime)spex.Datetime_watched;
                TimeSpan time = CompletedTime - StartTime;
                if (plan.Total_Time_Spent == null)
                {
                    plan.Total_Time_Spent = new TimeSpan();
                }
                plan.Total_Time_Spent = plan.Total_Time_Spent + time;
                Console.WriteLine(plan.Total_Time_Spent);
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return false;
            }
            return SaveDB(db);
        }

        //Change the user information
        public static bool editUser(long idUser, string name, string surname, DateTime birthday, string lang, string gender)
        {
            try
            {
                Planendo_Test_Entities db = Controlador.db;
                User c = db.User.Where(x => x.Id_User == idUser).FirstOrDefault();
                if (c != null)
                {
                    DateTime dt = new DateTime(1000, 1, 1);
                    c.Name = name;
                    c.Surname = surname;
                    if (birthday != null && birthday != dt)
                    {
                        c.Birthdate = (birthday).Date.ToString("dd/MM/yyyy");
                    }
                    c.Id_Gender = gender;
                    c.Default_Lang = lang.ToUpper();
                    return SaveDB(db);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return false;
            }

        }

        //Reiniciar els exercicis d'un dia en concret
        public static bool CleanOneDay(long idPlan, int numDay)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            Stored_Plans_x_Day day = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).FirstOrDefault();
            List<Stored_Plans_Exercises_x_Day> exDay = db.Stored_Plans_Exercises_x_Day.Where(z => z.Id_Day_Plan_Num == numDay && z.Id_Plan == idPlan).ToList();
            try
            {
                day.Status_Id = statusStarted;
                foreach (var ex in exDay)
                {
                    ex.Datetime_watched = null;
                    ex.Datetime_completed = null;
                }
            }
            catch (Exception e)
            {
                string w = e.Message.ToString();
                return false;
            }
            return SaveDB(db);
        }

        public static bool ChangePlanLike(long id_user, long id_plan)
        {
            Planendo_Test_Entities db = new Planendo_Test_Entities();
            try
            {
                var pla_like = db.Plans_Liked.Where(x => x.Id_Plan == id_plan && x.Id_User == id_user).FirstOrDefault();
                if (pla_like.Liked.Value) pla_like.Liked = false;                
                else pla_like.Liked = true;

                pla_like.Liked_Datetime = DateTime.Now;
                db.SaveChanges();
                return true;

            }
            catch (Exception e)
            {
                try
                {
                    Plans_Liked newPlanLike = new Plans_Liked();
                    newPlanLike.Id_Plan = id_plan;
                    newPlanLike.Id_User = id_user;
                    newPlanLike.Liked = true;
                    newPlanLike.Liked_Datetime = DateTime.Now;

                    db.Plans_Liked.Add(newPlanLike);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception err)
                {
                    string x = err.Message.ToString();
                    return false;
                }
            }
        }

    }
}