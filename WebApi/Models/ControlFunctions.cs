﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{

    public class ControlFunctions
    {
        private static Planendo_Test_Entities db = Controlador.db;

        public void PwdUpdatePassword(string email, string password)
        {
            try
            {
                db.PwdUpdateEmail(email, password);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
            }
        }
        public Boolean PwdValidateEmail(string email, string password)
        {
            try
            {
                bool control = new bool();
                control = Convert.ToBoolean(db.PwdValidateEmail(email, password).FirstOrDefault());
                return control;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return false;
            }
        }
        public long GetNewId()
        {
            long c = new long();
            c = (long)db.GetUniqueId().FirstOrDefault();
            return c;
        }
    }
}