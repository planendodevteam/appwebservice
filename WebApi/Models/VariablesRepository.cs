﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.ControllersDTO;


namespace WebApi.Models
{
    public class VariablesRepository
    {
        private static Planendo_Test_Entities db = Controlador.db;

        public static List<VariableDTO> searchListVariables(String filter)
        {
            try
            {
                List<Variables> list = db.Variables.Where(x => x.Id_Var.Contains(filter)).ToList();
                List<VariableDTO> listDTO = new List<VariableDTO>();
                foreach(Variables var in list)
                {
                    VariableDTO varDTO = new VariableDTO(var);
                    listDTO.Add(varDTO);
                }
                return listDTO;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }
    }
}