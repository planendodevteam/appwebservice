﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.ControllersDTO;

namespace WebApi.Models
{
    public class TagsRepository
    {
        //Connexió BBDD
        private static Planendo_Test_Entities db = Controlador.db;

        //list of All Stored_Exercise_x_Plan by id_Plan
        public static List<Tags_x_Plan> getListTagsxPlan(long idPlan)
        {
            try
            {
                List<Tags_x_Plan> tagList = db.Tags_x_Plan.Where(x => x.Id_Plan == idPlan).ToList();
                return tagList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
                return null;
            }
        }

        public static List<TagsDTO> getListTagsDTO(List<Tags_x_Plan> listTags)
        {
            try
            {
                List<TagsDTO> listTagsDTO = new List<TagsDTO>();
                foreach(Tags_x_Plan txp in listTags)
                {
                    TagsDTO tDTO = new TagsDTO(txp);
                    listTagsDTO.Add(tDTO);
                }

                return listTagsDTO;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
                return null;
            }
        }
    }
}