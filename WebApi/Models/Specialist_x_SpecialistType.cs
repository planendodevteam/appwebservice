//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Specialist_x_SpecialistType
    {
        public long Id_Specialist { get; set; }
        public int Id_SpecialistType { get; set; }
        public Nullable<bool> Enabled { get; set; }
    
        public virtual SpecialistTypes SpecialistTypes { get; set; }
        public virtual User User { get; set; }
    }
}
