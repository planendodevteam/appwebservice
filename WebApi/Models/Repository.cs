﻿using RCPT.AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.ControllersDTO;

namespace WebApi.Models
{
    public class Repository
    {
        //Connexió BBDD
        private static Planendo_Test_Entities db = Controlador.db;

        //Get Ex of Plan by idPlan
        public static List<Stored_Ex_Plan_DTO> AllExofPlan(int idPlan, string idLang)
        {
            List<Stored_Exercises_x_Plan> listEx = ExerciseRepository.getListSExPlan(idPlan);
            List<Stored_Ex_Plan_DTO> listExreturn = new List<Stored_Ex_Plan_DTO>();

            foreach (Stored_Exercises_x_Plan exs in listEx)
            {
                Stored_Ex_Plan_DTO ex = new Stored_Ex_Plan_DTO();
                ex = ExerciseRepository.ExsOfPlan(exs, idLang);
                if (ex != null) { listExreturn.Add(ex); }
            }
            return listExreturn;
        }

        public static String decodeData(string s)
        {
            try
            {
                string result;
                result = (s.Replace("%2D", "-").Replace("%5F", "_").Replace("%2E", ".").Replace("%21", "!").Replace("%7E", "~").Replace("%2A", "*").Replace("%27", "'").Replace("%28", "(").Replace("%29", ")"));
                return result;
            }
            catch (Exception e)
            {
                string ex = e.Message.ToString();
            }
            return "";
        }
    }
}