﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.ControllersDTO;

namespace WebApi.Models
{
    public class ProgramRepository
    {
        //Connexió BBDD
        private static Planendo_Test_Entities db = Controlador.db;

        //We get a list of the plans filtred by AccesLevel and ProgramType
        public static List<ProgramDTO> Get_plans_AccesLevel_ProgramType(string Value, string idLang)
        {
            try
            {
                List<Programs> lp = new List<Programs>();
                List<ProgramDTO> listProgram = new List<ProgramDTO>();

                long idSpecialist = db.User.Where(x => x.Name.Equals(Value)).Select(x => x.Id_User).FirstOrDefault();
                List<long> listIds = db.Programs_TXT.Where(x => x.Name.Equals(Value) && x.Id_Lang.Equals(idLang)).Select(x => x.Id_Program).ToList();

                if (idSpecialist != 0 && listIds != null)
                {
                    lp = db.Programs.Where(x => x.Created_by_Specialist == idSpecialist && x.Id_AccesLevel == 1).ToList();

                    foreach (long idProgram in listIds)
                    {
                        Programs p = db.Programs.Where(x => x.Id_Program == idProgram).FirstOrDefault();
                        if (p != null)
                        {
                            lp.Add(p);
                        }
                    }
                }
                else if (idSpecialist != 0)
                {
                    lp = db.Programs.Where(x => x.Created_by_Specialist == idSpecialist && x.Id_AccesLevel == 1).ToList();
                }
                else if (listIds != null)
                {
                    foreach (long idProgram in listIds)
                    {
                        Programs p = db.Programs.Where(x => x.Id_Program == idProgram).FirstOrDefault();
                        if (p != null)
                        {
                            lp.Add(p);
                        }
                    }
                }
                else
                {
                    return null;
                }

                return Get_InfoPrograms(lp, idLang);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public static List<ProgramDTO> Get_InfoPrograms(List<Programs> lp, string idLang)
        {
            try
            {
                List<ProgramDTO> listPrograms = new List<ProgramDTO>();
                foreach (Programs p in lp)
                {
                    ProgramDTO program = new ProgramDTO(p);
                    program.specialistName = db.User.Where(x => x.Id_User == program.idSpecialist).Select(x => x.Name).FirstOrDefault();
                    program.imgProgramType = db.ProgramTypes.Where(x => x.Id_ProgramType == p.Id_ProgramType).Select(x => x.Icon_Link).FirstOrDefault();
                    program.imgSpecialist = db.User.Where(x => x.Id_User == program.idSpecialist).Select(x => x.Photo).FirstOrDefault();
                    program.programTypeName = db.Programs_TXT.Where(x => x.Id_Program == program.idProgram && x.Id_Lang == idLang).Select(x => x.Name).FirstOrDefault();
                    if (program.specialistName != null) { program.specialistName = program.specialistName.Trim(); }
                    if (program.programTypeName != null) { program.programTypeName = program.programTypeName.Trim(); }
                    if (program.imgProgramType != null) { program.imgProgramType = program.imgProgramType.Trim(); }
                    if (program.imgSpecialist != null) { program.imgSpecialist = program.imgSpecialist.Trim(); }
                    program.lex = GetAllExsDay(1, program.idProgram, idLang);
                    listPrograms.Add(program);
                }
                return listPrograms;
            }
            catch (Exception e)
            {
                string s = e.Message.ToString();
                return null;
            }
        }

        //Assign program to a new user by the user
        public static bool? Assign_program(long idProgram, long idUser, int idProgression, int idLevel, int idLevelSeries, int numDays, bool? sequential)
        {
            try
            {
                Programs BDprogram = db.Programs.Where(x => x.Id_Program == idProgram).FirstOrDefault();
                if (BDprogram != null)
                {
                    Plans plan = new Plans();
                    ProgramDTO p = new ProgramDTO(BDprogram);
                    plan.Id_Plan = (long)db.GetUniqueId().FirstOrDefault();
                    plan.Id_Progression = idProgression;
                    plan.Id_User = idUser;
                    plan.Id_Level_series = idLevelSeries;
                    plan.Id_Level = idLevel;
                    plan.NumDays_Duration = numDays;
                    plan.Sequential = sequential;
                    plan.Id_ProgramType = p.idProgramType;
                    plan.Id_Company = p.idCompany;
                    plan.Id_Specialist = p.idSpecialist;
                    plan.Plan_Name_Stored = p.programName;
                    plan.Active = true;
                    plan.Created_Datetime = DateTime.Now;
                    plan.Id_Lang = db.User.Where(x => x.Id_User == idUser).Select(x => x.Default_Lang).FirstOrDefault();
                    plan.User_Name_Stored = db.User.Where(x => x.Id_User == idUser).Select(x => x.Surname).FirstOrDefault() + ", " + db.User.Where(x => x.Id_User == idUser).Select(x => x.Name).FirstOrDefault();
                    plan.Specialist_Name_Stored = db.User.Where(x => x.Id_User == p.idSpecialist).Select(x => x.Name).FirstOrDefault();
                    plan.Company_TradeName_Stored = p.companyName;
                    plan.Forecast_Start_Date = DateTime.Today;
                    plan.Forecast_End_Date = DateTime.Today.AddDays(numDays);
                    plan.Id_PlanStatus = 7;

                    db.Plans.Add(plan);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                string m = e.Message.ToString();
                return null;
            }
        }

        //Return all exercises of plan x day (p, d.idNumDay ,idLang, boolean); El controlador es per saber com omplirem l'informació 
        public static List<Stored_Ex_Plan_DTO> GetAllExsDay(int numDay, long idProgram, string idLang)
        {
            List<Stored_Ex_Plan_DTO> Exercises = new List<Stored_Ex_Plan_DTO>();
            try
            {
                foreach (var ex in db.Stored_Exercises_x_Plan.Where(x => x.Id_Program == idProgram)
                    .ToList()
                    .OrderBy(x => x.WorkPhases.OrderNum)
                    .OrderBy(x => x.order_id)
                    .ThenBy(x => x.Id_Work)
                    )
                {
                    Stored_Ex_Plan_DTO newEx = ExerciseRepository.GetInfoEx(ex, numDay, idLang);
                    if (newEx != null)
                        Exercises.Add(newEx);
                }
                //Si no son secuencials haurem de desordenarlos seguint l'estructura de workphase
                /*if (!db.Plans.Where(x => x.idP == idProgram).Select(x => x.Sequential).FirstOrDefault().Value)
                {
                    return ExerciseRepository.getOrderExercisesNoSecuencial(Exercises, numDay, idPlan, idLang);
                }*/
                return Exercises;
            }
            catch (Exception e)
            { string s = e.Message; return null; }
        }
    }
}