﻿using RCPT.AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Web;
using WebApi.ControllersDTO;

namespace WebApi.Models
{
    public class DiesRepository
    {
        //Connexió BBDD
        private static Planendo_Test_Entities db = Controlador.DB();

        private const int statusCompleted = 1;
        private const int statusStarted = 2;

        //Get List of Days (Plan, idLanguage, DayControlador) DayControlador: -True: Tota l'informació a través objecta Plan. -False: Sense llistat exercicis a través del objecta Plan.
        public static List<DayDTO> GetListDays(PlanDTO p, string idLang, bool ExControlador)
        {
            List<DayDTO> dies = new List<DayDTO>();
            try
            {

                //We get all information from Plan object
                for (int i = 0; i < p.DaysDuration; i++)
                {
                    DayDTO d;
                    //El Stored_Plans_x_Day d'on treurem la informació del nostra dia
                    Stored_Plans_x_Day Stored_day = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == p.IdPlan && x.Id_Day_Plan_Num == (i + 1)).FirstOrDefault();
                    if (Stored_day != null)
                    {
                        d = new DayDTO(Stored_day);
                        d.status_id = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == Stored_day.Id_Plan && x.Id_Day_Plan_Num == Stored_day.Id_Day_Plan_Num).Select(x => x.Status_Id).FirstOrDefault();
                        d.Daycompleted = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == Stored_day.Id_Plan && x.Id_Day_Plan_Num == Stored_day.Id_Day_Plan_Num).Select(x => x.Date_Completed).FirstOrDefault();
                        d.nameDay = db.Labels_TXT.Where(x => x.Id_Lang == idLang && x.Id_Label == "lbl_day").Select(z => z.Name).FirstOrDefault().Trim() + " " + d.idNumDay;
                        d.DayCompletedText = GetTextDayCompleted(d, idLang);
                        d.Total_Duration = db.Plans.Where(x => x.Id_Plan == Stored_day.Id_Plan).Select(x => x.Total_Time_Spent).FirstOrDefault();
                        d.Status = db.Plan_Day_Status.Where(x => x.Id_Status == d.status_id).Select(x => x.Name).FirstOrDefault();
                        //d.idPlanMaster = db.Plans.Where(x => x.Id_Plan == Stored_day.Id_Plan).Select(x => x.Id_Plan_Master).FirstOrDefault();

                        d.Feedback_user_dificulty = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == Stored_day.Id_Plan && x.Id_Day_Plan_Num == Stored_day.Id_Day_Plan_Num).Select(x => x.Feedback_User_Dificulty_Id).FirstOrDefault();
                        d.Feedback_user_rating = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == Stored_day.Id_Plan && x.Id_Day_Plan_Num == Stored_day.Id_Day_Plan_Num).Select(x => x.Feedback_User_Rating_Id).FirstOrDefault();
                        d.comment = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == Stored_day.Id_Plan && x.Id_Day_Plan_Num == Stored_day.Id_Day_Plan_Num).Select(x => x.Comment).FirstOrDefault();
                        d.audioName = db.Stored_Recording.Where(x => x.Id_Plan == Stored_day.Id_Plan && x.Id_Day_Plan_Num == Stored_day.Id_Day_Plan_Num).Select(x => x.AudioName).FirstOrDefault();
                        d.audioPath = db.Stored_Recording.Where(x => x.Id_Plan == Stored_day.Id_Plan && x.Id_Day_Plan_Num == Stored_day.Id_Day_Plan_Num).Select(x => x.AudioPath).FirstOrDefault();

                        if (d.Status != null) { d.Status = d.Status.Trim(); }
                        if (d.comment != null) { d.comment = d.comment.Trim(); }
                        if (d.audioName != null) { d.audioName = d.audioName.Trim(); }
                        if (d.audioPath != null) { d.audioPath = d.audioPath.Trim(); }
                    }
                    //Sino generem el dia amb l'info suficient
                    else
                        d = new DayDTO(p.IdPlan, (i + 1));

                    //Altres dades no inserides en la creació del nostra DayDTO
                    d.nameDay = db.Labels_TXT.Where(x => x.Id_Lang == idLang && x.Id_Label == "lbl_day").Select(z => z.Name).FirstOrDefault().Trim() + " " + d.idNumDay;
                    d.DayCompletedText = GetTextDayCompleted(d, idLang);
                    //Si ExControlador es igual a true obtindrem el llistat de exercicis a través del objecta PLAN  ||Mirem si es d'un plan Master o No
                    if (ExControlador)
                    {
                        d.lex = ExerciseRepository.GetAllExsDay(d.idNumDay, d.idPlan, idLang);
                        d.elements = GetDayElements(d.lex, idLang);
                    }
                    //Si ExControlador es false no obtindrem el llistat dels exercicis
                    else if (!ExControlador) { d.lex = null; }

                    if (d != null)
                    {
                        dies.Add(d);
                    }
                }
                //Retornem els list of Days
                return dies;
            }
            catch (Exception e)
            { string s = e.Message; return null; }
        }

        //Get information of one specific day
        public static DayDTO Get_Day(int numDay, long idPlan, string idLang, bool ExControlador)
        {
            try
            {
                DayDTO d;
                //El Stored_Plans_x_Day d'on treurem la informació del nostra dia
                Stored_Plans_x_Day Stored_day = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).FirstOrDefault();
                //d = new DayDTO();
                if (Stored_day != null)
                    d = new DayDTO(Stored_day);
                else
                    d = new DayDTO(idPlan, numDay);

                //Altres dades no inserides en la creació del nostra DayDTO
                d.status_id = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).Select(x => x.Status_Id).FirstOrDefault();
                d.Daycompleted = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).Select(x => x.Date_Completed).FirstOrDefault();
                d.nameDay = db.Labels_TXT.Where(x => x.Id_Lang == idLang && x.Id_Label == "lbl_day").Select(z => z.Name).FirstOrDefault().Trim() + " " + d.idNumDay;
                d.DayCompletedText = GetTextDayCompleted(d, idLang);
                d.Total_Duration = db.Plans.Where(x => x.Id_Plan == idPlan).Select(x => x.Total_Time_Spent).FirstOrDefault();
                d.Status = db.Plan_Day_Status.Where(x => x.Id_Status == d.status_id).Select(x => x.Name).FirstOrDefault();
                //d.idPlanMaster = db.Plans.Where(x => x.Id_Plan == idPlan).Select(x => x.Id_Plan_Master).FirstOrDefault();

                d.Feedback_user_dificulty = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).Select(x => x.Feedback_User_Dificulty_Id).FirstOrDefault();
                d.Feedback_user_rating = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).Select(x => x.Feedback_User_Rating_Id).FirstOrDefault();
                d.comment = db.Stored_Plans_x_Day.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).Select(x => x.Comment).FirstOrDefault();
                d.audioName = db.Stored_Recording.Where(x => x.Id_Plan == idPlan && x.Id_Day_Plan_Num == numDay).Select(x => x.AudioName).FirstOrDefault();
                if (d.audioName != null) { d.audioPath = db.Stored_Recording.Where(x => x.Id_Plan == Stored_day.Id_Plan && x.Id_Day_Plan_Num == Stored_day.Id_Day_Plan_Num).Select(x => x.AudioPath).FirstOrDefault(); }

                if (d.Status != null) { d.Status = d.Status.Trim(); }
                if (d.comment != null) { d.comment = d.comment.Trim(); }
                if (d.audioName != null) { d.audioName = d.audioName.Trim(); }
                if (d.audioPath != null) { d.audioPath = d.audioPath.Trim(); }

                if (d.Total_Duration == null)
                {
                    d.Total_Time_Spent = new TimeSpan(0, 0, 0).ToString(@"hh\:mm\:ss");
                }
                else
                {
                    d.Total_Time_Spent = d.Total_Duration.Value.ToString(@"hh\:mm\:ss");
                }

                //Si ExControlador es igual a true obtindrem el llistat de exercicis a través del objecta PLAN 
                if (ExControlador)
                {
                    d.lex = ExerciseRepository.GetAllExsDay(d.idNumDay, d.idPlan, idLang);
                    d.elements = GetDayElements(d.lex, idLang);
                }
                //Si ExControlador es false no obtindrem el llistat dels exercicis
                else if (!ExControlador) { d.lex = null; }

                return d;
            }
            catch (Exception e) { string s = e.Message; return null; }
        }

        /* RETURN THE AUDIO INFORMATION OF ONE SPECIFIC DAY */
        public static AudioDayDTO Get_AudioDay(int numDay, long idPlan)
        {
            try
            {
                Stored_Recording sr = db.Stored_Recording.Where(x => x.Id_Day_Plan_Num == numDay && x.Id_Plan == idPlan).FirstOrDefault();
                return new AudioDayDTO(sr);
            }
            catch (Exception e)
            {
                string s = e.Message;
                return null;
            }
        }

        //Get the text of Day Compelted (ex: The Date, Lunes 20 Enero)
        public static string GetTextDayCompleted(DayDTO d, string idLang)
        {
            var s = "";
            var l = "";
            if (idLang == "ESP") { l = "es-ES"; }
            else if (idLang == "CAT") { l = "ca-ES"; ; }
            else if (idLang == "ENG") { l = "en-EN"; ; }
            if (d.Daycompleted != null && d.Status != "Started")
            {
                CultureInfo culture = new CultureInfo(l);
                DateTime dt = d.Daycompleted.GetValueOrDefault();
                s = dt.ToString("dddd", culture) + " " + dt.Day + " " + dt.ToString("MMM", culture);
            }
            else if (d.status_id == statusStarted)
            {
                s = db.Labels_TXT.Where(x => x.Id_Lang == idLang && x.Id_Label == "lbl_follow").Select(z => z.Name).FirstOrDefault().Trim();
            }
            return s;
        }

        //Get elements of a day (list exercises and idlang)
        public static List<ElementsCS> GetDayElements(List<Stored_Ex_Plan_DTO> exercises, string idLang)
        {
            try
            {
                List<ElementsCS> elements = new List<ElementsCS>();

                foreach (Stored_Ex_Plan_DTO ex in exercises)
                {
                    Elements e = db.Elements_x_Exercise.Where(x => x.Id_Exercise == ex.idEx).Select(x => x.Elements).FirstOrDefault();
                    if (e != null)
                    {
                        if (elements.Where(x => x.idElement == e.Id_Element).FirstOrDefault() == null)
                        {
                            ElementsCS element = new ElementsCS();
                            element.idElement = e.Id_Element;
                            element.name = e.Elements_TXT.Where(x => x.Id_Lang.Equals(idLang)).Select(x => x.Name).FirstOrDefault().Trim();
                            element.weight = e.Weight;
                            elements.Add(element);
                        }
                    }
                }
                return elements;

            }
            catch (Exception e)
            {
                string s = e.Message.ToString();
                return null;
            }
        }
    }
}