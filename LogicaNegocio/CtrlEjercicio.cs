﻿using System;
using System.Collections.Generic;
using System.Linq;
using RCPT.AccesoDatos;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RCPT.LogicaNegocio
{
    public class CtrlEjercicio
    {
       private static rcpt2Entities1 db = new rcpt2Entities1();

       public ExerciseCS GetWork(string IdL,int IdEx)
        {
            ExerciseCS w = new ExerciseCS();
            Works ws = new Works();
            try
            {
                ws = db.Works.Where(x =>x.Id_Exercise == IdEx).First();
                // t.IdEjercicio = ex.Id_Exercise;
                w.IdUnits = (int) ws.Id_Units;
                w.IdExercise = (int)ws.Id_Exercise;
                w.IdWork = ws.Id_Work;
                w.linkPictureExerciseFemale = ws.Link_Picture_FemaleAdult;
                w.linkPictureExerciseMale = ws.Link_Picture_MaleAdult;
                w.linkVideoExerciseFemale = ws.Link_Video_FemaleAdult;
                w.linkVideoExerciseMale = ws.Link_Video_MaleAdult;
                w.SeriesNum = (int) ws.Series_Num;
                w.UnitsNum = (int)ws.Units_Num;
                w.WeightNum = ws.Weight_Num;
                w.name = ws.Works_TXT.Where(x => x.Id_Work == w.IdWork).Select(x => x.Name).First();
                w.observations = ws.Works_TXT.Where(x => x.Id_Work == w.IdWork).Select(x => x.Observations).First();
                

                return w;
            }
            catch (Exception exs)
            {
                string S = exs.Message.ToString();
                return w;
            }
        }
        public List<ExerciseCS> GetAll(string IdL)
        {
            List<ExerciseCS> p = new List<ExerciseCS>();
            List<Works> wrks = new List<Works>();
            try
            {
                wrks = db.Works.ToList();
                foreach (Works ws in wrks)
                {
                    ExerciseCS w = new ExerciseCS();

                    w.IdExercise = ws.Works_TXT.Where(x => x.Id_Lang == IdL).Select(x => x.Id_Work).First();
                    w.linkPictureExerciseMale = ws.Link_Picture_MaleAdult;
                    w.linkPictureExerciseFemale = ws.Link_Picture_FemaleAdult;
                    w.linkVideoExerciseFemale = ws.Link_Video_FemaleAdult;
                    w.linkVideoExerciseMale = ws.Link_Video_MaleAdult;
                    w.UnitsNum = (int)ws.Units_Num;
                    w.WeightNum = ws.Weight_Num;
                    w.CreatedBy = (int)ws.Created_By;
                    w.AccesLabel = ws.Id_AccessLevel;
                    w.name = ws.Works_TXT.Where(x => x.Id_Work == w.IdWork).Select(x => x.Name).First();
                    w.observations = ws.Works_TXT.Where(x => x.Id_Work == w.IdWork).Select(x => x.Observations).First();
                    w.isMaster = (bool)ws.IsMaster;


                    //c.IdEjercicio = ej.Id_Exercise;
                    //------ c.nombreEjercicio = ej.Works_TXT.Where(x => x.Id_Lang == IdL).Where(x => x.Id_Exercise == ej.Id_Exercise).
                    if (w.IdExercise != null) {
                        p.Add(w); }
                  
                   
    }
                return p;
            }
            catch (Exception ex)
            {
                string s = ex.Message.ToString();
                return p;
            }
        }
        public List<ExerciseCS> GetByName(string IdL, string name)
        {
            List<ExerciseCS> p = new List<ExerciseCS>();
            List<Works> wrks = new List<Works>();
            try
            {
               // wrks = db.Works_TXT.Where(x => x.Name.Contains(name)).ToList(); 
                wrks = db.Works.ToList();
                foreach (Works ws in wrks)
                {
                    ExerciseCS w = new ExerciseCS();
                    w.IdExercise = ws.Works_TXT.Where(x => x.Id_Lang == IdL).Select(x => x.Id_Work).First();
                    w.linkPictureExerciseMale = ws.Link_Picture_MaleAdult;
                    w.linkPictureExerciseFemale = ws.Link_Picture_FemaleAdult;
                    w.linkVideoExerciseFemale = ws.Link_Video_FemaleAdult;
                    w.linkVideoExerciseMale = ws.Link_Video_MaleAdult;
                    w.UnitsNum = (int)ws.Units_Num;
                    w.WeightNum = ws.Weight_Num;
                    w.CreatedBy = (int)ws.Created_By;
                    w.AccesLabel = ws.Id_AccessLevel;
                    w.name = ws.Works_TXT.Where(x => x.Id_Work == w.IdWork).Select(x => x.Name).First();
                    w.observations = ws.Works_TXT.Where(x => x.Id_Work == w.IdWork).Select(x => x.Observations).First();
                    w.isMaster = (bool)ws.IsMaster;

                    //c.IdEjercicio = ej.Id_Exercise;
                    //------ c.nombreEjercicio = ej.Works_TXT.Where(x => x.Id_Lang == IdL).Where(x => x.Id_Exercise == ej.Id_Exercise).
                    if (w.IdExercise != null) { p.Add(w); }

                }
                return p;
            }
            catch (Exception ex)
            {
                string s = ex.Message.ToString();
                return p;
            }
        }


        public int Save(string nombreEjercicio, string linkEjercicioMujer, string linkEjercicioHombre, string linkFotoEjercicioMujer, string linkFotoEjercicioHombre, string IdL)
        {
            int p = 0;
            Works w = new Works();
            w.Link_Picture_FemaleAdult = linkFotoEjercicioMujer;
            w.Link_Picture_MaleAdult = linkFotoEjercicioHombre;
            w.Link_Video_FemaleAdult = linkEjercicioMujer;
            w.Link_Video_MaleAdult = linkEjercicioHombre;
            //db.Works.AddObject(e);

            Works_TXT ext = new Works_TXT();
            ext.Id_Lang = IdL;
            ext.Name = nombreEjercicio;
           // ext.Text = nombreEjercicio;

            w.Works_TXT.Add(ext);
            try
            {

                db.SaveChanges();
                return p;
            }
            catch (Exception ex)
            {
                string S = ex.Message.ToString();
                return p;
            }
        }
        public int Update(string nombreEjercicio, string linkEjercicioMujer, string linkEjercicioHombre, string linkFotoEjercicioMujer, string linkFotoEjercicioHombre, string IdL, int ejId)
        {
            int p = 0;
            Works w = new Works();
            w = db.Works.Where(x => x.Id_Exercise == ejId).First();
            w.Link_Picture_FemaleAdult = linkFotoEjercicioMujer.Trim();
            w.Link_Picture_MaleAdult = linkFotoEjercicioHombre.Trim();
            w.Link_Video_FemaleAdult = linkEjercicioMujer.Trim();
            w.Link_Video_MaleAdult = linkEjercicioHombre.Trim();

            Works_TXT ext = new Works_TXT();
            //ext = db.Works_TXT.Where(x => x.Id_Exercise == ejId).Where(x => x.Id_Lang == IdL).First();
            if (ext != null)
            {
                ext.Id_Lang = IdL;
                ext.Name = nombreEjercicio.Trim();
                //ext.Text = nombreEjercicio.Trim();
            }
            else
            {
                ext.Id_Lang = IdL;
                ext.Name = nombreEjercicio.Trim();
                //ext.Text = nombreEjercicio.Trim();
                w.Works_TXT.Add(ext);
            }

            try
            {
                db.SaveChanges();
                return p;
            }
            catch (Exception ex)
            {
                string S = ex.Message.ToString();
                return p;
            }
        }

    }
}
