﻿using RCPT.AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RCPT.LogicaNegocio
{
    public class CtrlIdioma
    {

        private static rcptEntities1 db = new rcptEntities1();
        public static List<Idioma> GetAll()
        {
            List<Idioma> p = new List<Idioma>();
            List<Languages> e = new List<Languages>();
            try
            {
                e = db.Languages.ToList();
                foreach (Languages l in e)
                {
                    Idioma c = new Idioma();
                    c.IdL = l.Id_Lang;
                    c.nombre = l.Name;
                    p.Add(c);
                }
                return p;
            }
            catch (Exception ex)
            {
                string s = ex.Message.ToString();
                return p;
            }
        }
    }
}
