﻿using System;
using System.Web.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RCPT.AccesoDatos;
using RCPT.LogicaNegocio;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace RCPT_WebApp
{
    public partial class postData : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }
      
        [WebMethod]
        public static int saveTrabajo(List<List<string>> dias)
        {
            int x = 0;
            foreach (List<string> s in dias) {
                Dia dia = new Dia();

            }
            try
            {
                
                return x;

            }
            catch (Exception ex)
            {
                return x;
            }
        }
        [WebMethod]
        public static BodyPart getTabajo(int id)
        {
            BodyPart trabajo = new BodyPart();

            try
            {
                return trabajo;

            }
            catch (Exception ex)
            {
                return trabajo;
            }
        }
        [WebMethod]
        public static List<BodyPart> getTabajos()
        {
            List<BodyPart> Tlist = new List<BodyPart>();

            try
            {
                return Tlist;

            }
            catch (Exception ex)
            {
                return Tlist;
            }
        }
        [WebMethod]
        public static int updEjercicio(string nombreEjercicio, string linkEjercicioMujer, string linkEjercicioHombre, string linkFotoEjercicioMujer, string linkFotoEjercicioHombre, string IdL, int ejId)
        {
            int x = 0;
            CtrlEjercicio e = new CtrlEjercicio();


            try
            {
                x = e.Update(nombreEjercicio, linkEjercicioMujer, linkEjercicioHombre, linkFotoEjercicioMujer, linkFotoEjercicioHombre, IdL, ejId);
                return x;

            }
            catch (Exception ex)
            {
                return x;
            }
        }
        [WebMethod]
        public static int saveEjercicio(string nombreEjercicio, string linkEjercicioMujer, string linkEjercicioHombre, string linkFotoEjercicioMujer, string linkFotoEjercicioHombre, string IdL)
        {
            int x = 0;
            CtrlEjercicio e = new CtrlEjercicio();


            try
            {
                x = e.Save(nombreEjercicio, linkEjercicioMujer, linkEjercicioHombre, linkFotoEjercicioMujer, linkFotoEjercicioHombre, IdL);
                return x;

            }
            catch (Exception ex)
            {
                return x;
            }
        }
        [WebMethod]
        public static List<Work> getEjercicios(string IdL)
        {
            List<Work> list = new List<Work>();
            CtrlEjercicio c = new CtrlEjercicio();

            try
            {
                list = c.GetAll(IdL);
                return list;

            }
            catch (Exception ex)
            {
                return list;
            }
        }
        public static string getEjercicioss()
        {
            List<Work> list = new List<Work>();
            CtrlEjercicio c = new CtrlEjercicio();
            string IdL = "ESP";
            var json = "";
            try
            {
                var jsonSerialiser = new JavaScriptSerializer();
                
                list = c.GetAll(IdL);
                json = jsonSerialiser.Serialize(list);
                return "{\"Work\":[{\"id\":\"1\",\"fullname\":\"" + "Eric"  + "\",\"linkPictureExerciseMale\":\"https://www.google.com/search?q=foto+ejercicio&rlz=1C1AVFC_enES821ES822&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiszobv-IDgAhWr4IUKHVftBcgQ_AUIDigB&biw=1288&bih=640#imgrc=nmKTPCazTdpnNM: \"}]}"; ;

            }
            catch (Exception ex)
            {
                return json;
            }
        }
      /*[WebMethod]
      public static Work getEjercicio(string IdL, int ejId)
        {
            Work list = new Work();
            CtrlEjercicio c = new CtrlEjercicio();

            try
            {
                list = c.Get(IdL, ejId);
                return list;

            }
            catch (Exception ex)
            {
                return list;
            }
        }*/

        [WebMethod]
        public static int saveGT(string nombreGT)
        {
            int x = 0;
            
            try
            {
                return x;

            }
            catch (Exception ex)
            {
                return x;
            }
        }
        [WebMethod]
        public static List<GrupoTrabajo> getGT()
        {
            List<GrupoTrabajo> GTlist = new List<GrupoTrabajo>();

            try
            {
                return GTlist;

            }
            catch (Exception ex)
            {
                return GTlist;
            }
        }

        [WebMethod]
        public static List<Idioma> getIdiomas()
        {
            List<Idioma> list = new List<Idioma>();
            CtrlIdioma c = new CtrlIdioma();
            try
            {
                list = c.GetAll();
                return list;

            }
            catch (Exception ex)
            {
                return list;
            }
        }


    }
}