﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCPT.AccesoDatos
{
    public class changeDateExCS
    {
        public long idPlan { get; set; }
        public int numDay { get; set; }
        public int idWP { get; set; }
        public long idEx { get; set; }
        public long idProgram { get; set; }
        public int idWork { get; set; }
    }
}
