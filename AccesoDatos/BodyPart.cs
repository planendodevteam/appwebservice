﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCPT.AccesoDatos
{
    public class BodyPart
    {
        public int idBodyPart { get; set; }
        public string OrderNum { get; set; }
    }
}
