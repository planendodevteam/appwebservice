﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCPT.AccesoDatos
{
    public class UserCS
    {
        public long idUser { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string fullname { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string birthdate { get; set; }
        public string gender { get; set; }
        public string lang { get; set; }
        public string welcome { get; set; }
        public string img { get; set; }
        public bool isActive { get; set; }
    }
}
