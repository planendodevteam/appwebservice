﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCPT.AccesoDatos
{
    public class ElementsCS
    {
        public int idElement { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public bool? weight { get; set; }

    }
}
