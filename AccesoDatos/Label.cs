﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCPT.AccesoDatos
{
    public class Label
    {
        public string idL { get; set; }
        public string idLabel { get; set; }
        public string Name { get; set; }
    }
}
