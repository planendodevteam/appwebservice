﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCPT.AccesoDatos
{
    public class Workcs
    {
        public int IdWork { get; set; }
        public string name { get; set; }
        public string observations { get; set; }
        public int IdExercise { get; set; }
        public int IdUnits { get; set; }
        public int SeriesNum { get; set; }
        public string WeightNum { get; set; }
        public int UnitsNum { get; set; }
        public int CreatedBy { get; set; }
        public string AccesLabel { get; set; }
        public string linkVideoExerciseMale { get; set; }
        public string linkVideoExerciseFemale { get; set; }
        public string linkPictureExerciseMale { get; set; }
        public string linkPictureExerciseFemale { get; set; }
        public bool isMaster { get; set; }


    }
}
