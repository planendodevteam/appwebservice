﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCPT.AccesoDatos
{
    public class DayFeedBackCS
    {
        public long idPlan { get; set; }
        public int numDay { get; set; }
        public int valoration { get; set; }
        public int difficulty { get; set; }
        public string comment { get; set; }
        public string audioName { get; set; }
        public string audioPath { get; set; }
        public string audioFile { get; set; }
    }
}
