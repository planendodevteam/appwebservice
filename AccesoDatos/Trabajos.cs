﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCPT.AccesoDatos
{
    public class Trabajos
    {
        public Dias dias { get; set; }
        public string nombreTrabajo { get; set; }
        public int IdTrabajo { get; set; }
        public int IdUnidades { get; set; }
        public int IdEjercicio { get; set; }
        public int IdGT { get; set; }
        public string CreadoPor { get; set; }
        public float NumeroPeso { get; set; }
        public int NumeroUnidades { get; set; }
        public int NumeroSeries { get; set; }
        public string LinkVideoHombreAdulto { get; set; }
        public string LinkVideoMujerAdulta { get; set; }
        public string LinkFotoHombreAdulto { get; set; }
        public string LinkFotoMujerAdulta { get; set; }

    }
}
