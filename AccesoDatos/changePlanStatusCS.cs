﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCPT.AccesoDatos
{
    public class changePlanStatusCS
    {
        public long idUser { get; set; }
        public long idPlan { get; set; }
        public int status { get; set; }
        public int date { get; set; }
    }
}
