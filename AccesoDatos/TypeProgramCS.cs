﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCPT.AccesoDatos
{
    public class TypeProgramCS
    {
        public int id { get; set; }
        public string icon { get; set; }
        public string name { get; set; }
        public string value { get; set; }


        public TypeProgramCS(int id, string icon, string name, string value)
        {
            this.id = id;
            this.icon = icon.Trim();
            this.name = name.Trim();
            this.value = value.Trim();
        }
    }
}
